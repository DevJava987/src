package com.designpattern.BehaviouralDesignPatterns.Observer;

import java.util.Observable;
import java.util.Observer;

public class Mylisteners implements Observer {

	@Override
	public void update(Observable observable, Object arg1) {
		if (arg1 instanceof String && observable instanceof Subject){
			System.out.println("notification received from "+((Subject)observable).getIdendity());
			System.out.println("she wanna observ a "+arg1);
		}

	}

}
