package com.designpattern.StructuralDesignPatterns.Proxy;

public interface Service {

	public void runCommand();	
}
