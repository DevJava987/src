package com.designpattern.StructuralDesignPatterns.Delegate.SimpleExample;

public class C implements I {

	private I i;
	
	public C(I i){
		this.i = i;
	}
	
	public void switchTo (I i){
		this.i = i;
	}

	@Override
	public void doTask() {
		i.doTask();
	}

}
