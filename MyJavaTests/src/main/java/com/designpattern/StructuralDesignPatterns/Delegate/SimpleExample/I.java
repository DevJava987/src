package com.designpattern.StructuralDesignPatterns.Delegate.SimpleExample;

public interface I {

	public void doTask();
	
}
