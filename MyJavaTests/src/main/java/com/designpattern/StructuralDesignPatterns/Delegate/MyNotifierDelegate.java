package com.designpattern.StructuralDesignPatterns.Delegate;

public class MyNotifierDelegate implements NotificationService {

	private NotificationService notificationService;
	private ServiceLookUp serviceLookUp;
	
	public MyNotifierDelegate(){
		serviceLookUp = new ServiceLookUp();
	}
	
	@Override
	//this method will delegate the task to the notificationService
	public void notifyClients() {
		notificationService = serviceLookUp.getNotificationService();
		notificationService.notifyClients();
	}

}
