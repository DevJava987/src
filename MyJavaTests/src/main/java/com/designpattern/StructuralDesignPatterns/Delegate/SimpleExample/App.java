package com.designpattern.StructuralDesignPatterns.Delegate.SimpleExample;

public class App {

	public static void main (String[] args){
		A a = new A();
		C c = new C(a);
		c.doTask();
		
		B b = new B();
		c.switchTo(b);
		c.doTask();
		
	}
	
}
