package com.designpattern.StructuralDesignPatterns.Composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * un objet composite est constitu� d'un ou de plusieurs objets similaires
 * ayant des fonctionnalit�s similaires: impl�mentant la m�me interface. 
 * L'id�e est de manipuler un groupe d'objets de la m�me fa�on que s'il
 * s'agissait d'un seul objet.
 * Les objets ainsi regroup�s doivent poss�der des op�rations communes,
 * c'est-�-dire un "d�nominateur commun".
 */
public class CompositeComponent implements Component{

	private List<Component> components = new ArrayList<>(); 
	
	@Override
	public void doTask() {
		for (Iterator<Component> iterator = components.iterator(); iterator.hasNext();) {
			Component c = iterator.next();
			c.doTask();
		}
	}
	
	/**
	 * le composite doit propposer une m�thode lui
	 * permettant de s'ajouter de nouveaux elements
	 * @param c
	 */
	public void addComponent (Component c){
		components.add(c);
	}

	/**
	 * le composite doit propposer une m�thode
	 * permettant de supprimer des elements
	 * @param c
	 */
	public void removeComponent(Component c){
		components.remove(c);
	}

}
