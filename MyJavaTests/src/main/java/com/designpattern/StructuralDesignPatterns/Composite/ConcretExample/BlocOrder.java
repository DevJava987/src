package com.designpattern.StructuralDesignPatterns.Composite.ConcretExample;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BlocOrder implements Order {

	private Set<Order> orders = new HashSet<>();
	
	@Override
	public void execute(int qty) {
		for (Iterator<Order> iterator = orders.iterator(); iterator.hasNext();) {
			Order o = iterator.next();
			o.execute(qty);
		}
	}
	
	public void addOrder(Order o){
		orders.add(o);
	}
	
	public void removeOrder(Order o){
		orders.remove(o);
	}

}
