package com.designpattern.StructuralDesignPatterns.Composite.ConcretExample;

public interface Order {

	public void execute(int qty);
}
