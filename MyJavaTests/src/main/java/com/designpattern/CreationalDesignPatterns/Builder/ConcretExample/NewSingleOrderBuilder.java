package com.designpattern.CreationalDesignPatterns.Builder.ConcretExample;

public class NewSingleOrderBuilder extends FixMessageBuilder {

	@Override
	public void buildHeader() {
		// build the header here
	}

	@Override
	public void buildBody() {
		// build the body here
	}

	@Override
	public void buildTail() {
		// build the tail here
	}
}
