package com.designpattern.CreationalDesignPatterns.Builder;

public class BuilderType2 extends Builder {

	@Override
	void buildPart1() {
		// call the setPart1 here with right params
	}

	@Override
	void buildPart2() {
		//  call the setPart2 here with right params
	}

	@Override
	void buildPart3() {
		//  call the setPart3 here with right params
	}

}
