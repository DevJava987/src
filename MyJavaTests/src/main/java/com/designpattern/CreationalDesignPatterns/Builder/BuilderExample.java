package com.designpattern.CreationalDesignPatterns.Builder;

public class BuilderExample {

	public static void main ( String ...args){
		
		Director dir = new Director();
		
		Builder builderType1 = new BuilderType1();
		Builder builderType2 = new BuilderType2();
		
		//building object using the 1uilderType1
		dir.setBuilder(builderType1);
		dir.Construct();
		dir.getObjectToBuild();

		//building object using the 1uilderType2
		dir.setBuilder(builderType2);
		dir.Construct();
		dir.getObjectToBuild();

	}
	
}
