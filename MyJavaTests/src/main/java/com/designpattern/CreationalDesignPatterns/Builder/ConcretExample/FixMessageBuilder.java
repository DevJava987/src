package com.designpattern.CreationalDesignPatterns.Builder.ConcretExample;


/**
 * Builder pattern builds a COMPLEX object using simple objects
 * and using a STEP BY STEP APPROACH. This type of design pattern
 * comes under creational pattern as this pattern provides one of 
 * the best ways to create an object.
 */

public abstract class FixMessageBuilder {
	
	//the builder will create this object
	private FixMessage fixMessage;
	
	public FixMessage getFixMessage(){
		return fixMessage;
	}
	
	public void createFixMessage(){
		fixMessage = new FixMessage();
	}
	
	abstract public void buildHeader();
	abstract public void buildBody();
	abstract public void buildTail();
}
