package com.designpattern.CreationalDesignPatterns.Builder;

public class Director {
	
	//here the absrtact type is used
	private Builder builder ;
	
	//for the parameter we use the absract type
	public void setBuilder(Builder bld){
		this.builder = bld;
	}
	
	public void Construct(){
		this.builder.createNewObjectTobuild();
		this.builder.buildPart1();
		this.builder.buildPart2();
		this.builder.buildPart3();
	}
	
	public ObjectToBuild getObjectToBuild(){
		return this.builder.getObjectToBuild();
	}

}
