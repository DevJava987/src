package com.swing.MyFrame;

public class Person {

	Integer id;
	String firstName;
	String lastName ;
    String sport;
	Integer age;
	Boolean vegetarian;
	
	public Person (Integer id, String firstName ,String lastName,String sport,Integer age, Boolean vegetarian){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sport = sport;
		this.vegetarian = vegetarian;
		this.age = age;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSport() {
		return sport;
	}
	public void setSport(String sport) {
		this.sport = sport;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public boolean isVegetarian() {
		return vegetarian;
	}
	public void setVegetarian(boolean vegetarian) {
		this.vegetarian = vegetarian;
	}
}
