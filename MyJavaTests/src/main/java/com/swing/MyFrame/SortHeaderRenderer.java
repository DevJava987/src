package com.swing.MyFrame;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;

/***
 * Renders a small arrow next to the title in the column header<br/>
 * <br/>
 * Original code by Claude Duguay for "Java Pro"<br/>
 * JSortTable can be found here:<br/>
 * <a href="http://www.fawcette.com/javapro/2002_08/magazine/columns/visualcomponents/default_pf.aspx">
 * http://www.fawcette.com/javapro/2002_08/magazine/columns/visualcomponents/default_pf.aspx
 * </a><br/>
 * <br/>
 * - Changed to make the 'sortedColumnIndex' reflect the model's index
 *   instead of the 'visible' index. When you dragged a column, the arrow
 *   stuck with the 'x-th' column, even though you dragged it to another place.
 *   Now the renderer compares the index of the column in the model and it
 *   'sticks' to the column being dragged.<br/>
 * 
 * @author Claude Duguay
 * @author Vincent Vollers
 */
public class SortHeaderRenderer extends DefaultTableCellRenderer {
	
	/***
	 * The icon which is put next to an unsorted column (empty)
	 */
	public SortFilterSwitchIcon NONSORTED = new SortFilterSwitchIcon(SortableArrowIcon.NONE);
	
	/***
	 * The icon which is put next to a column sorted ascending
	 */
	private SortFilterSwitchIcon ASCENDING = new SortFilterSwitchIcon(SortableArrowIcon.ASCENDING);
	
	/***
	 * The icon which is put next to a column sorted descending
	 */
	private SortFilterSwitchIcon DECENDING = new SortFilterSwitchIcon(SortableArrowIcon.DECENDING);

	/***
	 * Creates a new SortTreeRenderer, basicly sets up the text-alignment.
	 */
	public SortHeaderRenderer() {
		setHorizontalTextPosition(LEFT);
		setHorizontalAlignment(CENTER);
	}

	/***
	 * Updates the component to reflect the current header
	 */
    @SuppressWarnings("unchecked")
    @Override
	public Component getTableCellRendererComponent(
		JTable table,
		Object value,
		boolean isSelected,
		boolean hasFocus,
		int row,
		int col) {
		
		// we'll be using these to determine if the header is attached to a sorting column
		int[] indexes = null;
		boolean[] ascendings = null;
		boolean filtered = false;
		
		// we'll use the modelIndex instead of 'col', since 'col' is the
		// index of the column on screen and 'modelCol' is the index of
		// the column in the model.
		int modelCol = table.getColumnModel().getColumn(col).getModelIndex();
				
		// I would be surprised if this renderer is used with a non-JSortTable
		// but anyway, this code gets the current selected column-index and order.
		
		//if (table instanceof JSortTable) {
			JTable sortTable = (JTable) table;
//			
//			indexes = sortTable.getSortColumns();
//			ascendings = sortTable.getSortAscending();
//			filtered = sortTable.isFilteredColumn(modelCol);
//		//}
//		
//		
//		// set the header font and colors
//		if (table != null) {
//			JTableHeader header = table.getTableHeader();
//			if (header != null) {
//				setForeground(header.getForeground());
//				setBackground(header.getBackground());
//				
//				setFont(header.getFont());
//			}
//		}
//		
//		// set the icon for this column (notice: modelCol is the
//		// index of the column in the model for the current column,
//		// 'index' is the selected column)
//		SortFilterSwitchIcon icon;
//		int idx = findSortedColumn(modelCol,indexes);
//		if(idx>=0) {
//			icon = ascendings[idx] ? ASCENDING : DECENDING;
//			icon.setLevel(idx);
//		}
//		else icon=NONSORTED;
//
//		icon.setFiltered(filtered);
//			
//		// set the text of the header
//		String text = (value == null) ? "" : value.toString();
//		setText(text);
//		
//		if(text.equals("")) 
//			setToolTipText(null);
//		else 
//			setToolTipText(text);
//
//		
//		TableModel model = table.getModel();	
//		if ( model instanceof TreeTableModelAdapter ) {
//			SortTableColumnsDescriptor desc =  ((TreeTableModelAdapter<TreeTableNode>) model).getColumnsDescriptor();
//			if ( desc.isColumnSwitchable(modelCol) )
//				icon.setSwitchable(true);
//			else
//				icon.setSwitchable(false);
//			
//			setToolTipText(desc.getColumnToolTip(modelCol));
//						
//		}
//	
//		setIcon(icon);
//		
		
		// set the border of the header
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
		return this;
	}
	
	private int findSortedColumn(int modelCol,int[] indexes) {
		for (int i = 0; i < (indexes==null ? 0 :indexes.length); i++) {
			if(indexes[i]==modelCol) return i;
		}
		return -1;
	}
}
