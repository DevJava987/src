package com.swing.MyFrame;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MyListSelectionListener implements ListSelectionListener {

	MyJTable table;
	
	public MyListSelectionListener(MyJTable myTable) {
		table = myTable;
	}
	public void valueChanged(ListSelectionEvent event) {
		int viewRow = table.getSelectedRow();
		if (viewRow < 0) {
			//Selection got filtered away.
			System.out.println("viewRow < 0");
		} else {
			System.out.println("selected row: "+table.getSelectedRow());
			Object[] o = ((Object[][])((MyTableModel)table.getModel()).getData())[table.getSelectedRow()];
			Person p = (Person)(((MyTableModel)table.getModel()).getDataMap()).get(o[0]);
			System.out.println(o[0]);
			System.out.println(o[1]);
			System.out.println(o[2]);
			System.out.println(p);
		}
	}

	
}
