package com.swing.MyFrame;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JTable;
import javax.swing.table.TableColumnModel;


/**
 * 
 * This class manage de switching between columns 
 * @author casanova
 *
 */
public class SwitchableColumnMouseAdapter extends MouseAdapter implements MouseMotionListener {
    protected JTable table;
    protected int col; //the col that have to switch in the col_model 
	private Color highlightColor;
    
    
	public SwitchableColumnMouseAdapter( JTable table , int col, Color highlightColor){
		this.col = col;
		this.table = table;
		this.highlightColor=highlightColor;
	}
		
	// We do this to prevent cancel cell editing by clicking on header
    @Override
	public void mousePressed(MouseEvent e){
		if (table.isEditing()){ 
			table.getCellEditor().stopCellEditing();
		}
	}
	
	// When click on column header is done !
    @Override
	public void mouseClicked(MouseEvent event) { 

		if( event.getButton() == MouseEvent.BUTTON3 ) {
			TableColumnModel colModel = table.getColumnModel(); 
			 
			// get the visible column index at the current mouse coordinates
			int index 		= colModel.getColumnIndexAtX(event.getX());
			int modelIndex 	= colModel.getColumn(index).getModelIndex();
			int indexVue 	= table.convertColumnIndexToView(modelIndex);

			if (true/*isSwithable*/){
				//nvelle colonne � afficher!!!
				int newCol = 3;
				colModel.getColumn(indexVue).setHeaderValue("colonne apr�s switch");
				colModel.getColumn(indexVue).setCellRenderer(new HighlightableHeaderRenderer());

				table.getTableHeader().repaint();
			}
			onClick(); 
			table.repaint();
		}
    }
	
	public void onClick(){/*empty block*/}

    @Override
	public void mouseMoved(MouseEvent e) {
		TableColumnModel colModel = table.getColumnModel();
		
		// get the visible column index at the current mouse coordinates
		int index = colModel.getColumnIndexAtX(e.getX());
		if (index == -1) return;
		
		// get the index of that column in the model
		int modelIndex 	= colModel.getColumn(index).getModelIndex();
		
        if(modelIndex==col) {
            int index2 = table.convertColumnIndexToView(modelIndex);
            if (index2 == -1) return;
            HighlightableHeaderRenderer r = (HighlightableHeaderRenderer) table.getColumnModel().getColumn(index2).getHeaderRenderer();
            if(r != null && r.isDefaultHighlightColor()) {
              r.setHighlightColor(highlightColor);
              table.getTableHeader().repaint();
          }
        } 
        else {
            this.deselectColumn(col);
        }
	}

    @Override
	public void mouseDragged(MouseEvent event) {/*empty block*/}
	
    private void deselectColumn(int colP) {
        int index = table.convertColumnIndexToView(colP);
        if (index == -1) return;   
        
        HighlightableHeaderRenderer r = (HighlightableHeaderRenderer)table.getColumnModel().getColumn(index).getHeaderRenderer();
		if( r!= null && !r.isDefaultHighlightColor()) {
			r.setDefaultHighlightColor();
			table.getTableHeader().repaint();
		}
    }

    @Override
    public void mouseExited(MouseEvent e){
    	deselectColumn(col);
    }
    @Override
    public void mouseEntered(MouseEvent e){
    	mouseMoved(e);
    }

    

}
