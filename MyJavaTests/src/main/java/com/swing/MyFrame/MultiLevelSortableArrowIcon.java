/*
 * Created on 21 juin 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.swing.MyFrame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;

/**
 * @author tropenat_f
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MultiLevelSortableArrowIcon extends SortableArrowIcon {
	protected Font levelFont = new Font("Arial",Font.BOLD,10);
	protected int level;
	protected int xlevel=7;
	protected int ylevel=9;
	
	/**
	 * @param direction
	 */
	public MultiLevelSortableArrowIcon(int direction) {
		this(direction,-1);
	}	
	
	public MultiLevelSortableArrowIcon(int direction,int level) {
		super(direction);
		setLevel(level);
	}
    @Override
	public int getIconWidth() {
		if(level!=-1) return super.getIconWidth()+xlevel;
        return super.getIconWidth();
	}

    @Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		super.paintIcon(c,g,x,y);
		if(direction!=NONE&&level!=-1) {
			g.setColor(Color.blue);	
			Font f = g.getFont();	
			g.setFont(levelFont);
			g.drawString(String.valueOf(level+1),x+xlevel,y+ylevel);
			g.setFont(f);
		}
	}
	/**
	 * @return
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param i
	 */
	public void setLevel(int i) {
		level = i;
	}

}