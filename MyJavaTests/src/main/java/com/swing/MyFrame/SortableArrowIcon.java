package com.swing.MyFrame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

/***
 * Draws a small arrow.<br/>
 * <br/>
 * Original code by Claude Duguay for "Java Pro"<br/>
 * 
 * @author Claude Duguay, tropenat_f
 * 
 */

public class SortableArrowIcon implements Icon {
  public static final int NONE = 0;
  public static final int DECENDING = 1;
  public static final int ASCENDING = 2;

  protected int direction;
  protected int width = 9;
  protected int height = 9;
  
  public SortableArrowIcon(int direction) {
  	this.direction = direction;
  }
  
  public int getIconWidth() {
    if(direction!=NONE) return width;
    return 0;
  }
  
  public int getIconHeight() {
  	if(direction!=NONE) return height;
    return 0;
  }
  
  public void paintIcon(Component c, Graphics g, int x, int y) {
  	if(direction!=NONE) {
	    Color bg = c.getBackground();
	    Color light = bg.brighter();
	    Color shade = bg.darker(); //.darker();
	  
	    int w = width;
	    int h = height;
	    int m = w / 2;
	    if (direction == DECENDING) {
	      g.setColor(shade);
	      g.drawLine(x, y, x + w, y);
	      g.drawLine(x, y, x + m, y + h);
	      g.setColor(light);
	      g.drawLine(x + w, y, x + m, y + h);
	    }
	    if (direction == ASCENDING) {
	      g.setColor(shade);
	      g.drawLine(x + m, y, x, y + h);
	      g.setColor(light);
	      g.drawLine(x, y + h, x + w, y + h);
	      g.drawLine(x + m, y, x + w, y + h);
	    }
  	}
  }

	/**
	 * @param direction The direction to set.
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}
}

