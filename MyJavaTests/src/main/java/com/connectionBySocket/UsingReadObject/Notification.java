package com.connectionBySocket.UsingReadObject;

import java.io.Serializable;


public class Notification implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NOTIF_TYPE = "NOTIF";
	public static final String CLOSE_TYPE = "CLOSE";
	private String type = NOTIF_TYPE;
	private String name;
	private int age;

	Notification(String name, int age){
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString(){
		return name +" "+age+ " "+type;
	}
	
	void setType(String type){
		this.type = type;
	}
	
	public String getType(){
		return type;
	}
}
