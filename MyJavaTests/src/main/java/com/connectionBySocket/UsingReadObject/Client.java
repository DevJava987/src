package com.connectionBySocket.UsingReadObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {

	public static void main (String[] args){
		
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }
		
		String hostname = args[0];
		int serverPort = Integer.parseInt(args[1]);

		try(Socket socket = new Socket(hostname, serverPort);
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());) {
			
			out.writeObject(new Notification("name",1));
			out.writeObject(new Notification("name",2));
			out.writeObject(new Notification("name",3));
			out.writeObject(new Notification("name",4));
			out.flush();
			
			System.out.println("waiting for serveru ack");
			Object ack = in.readObject();
			System.out.println(ack);
			
			Thread.sleep(20000);
			
		} catch ( IOException | ClassNotFoundException| InterruptedException e) {
			e.printStackTrace();
		} 

		
		
	}
	
}
