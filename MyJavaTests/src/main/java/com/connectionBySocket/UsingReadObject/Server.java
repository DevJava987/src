package com.connectionBySocket.UsingReadObject;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Server extends Thread{

	private Map<Integer,Socket> socketByClientId = new HashMap<>();
	private Map<Integer,ObjectOutputStream> outputStreamByClientId = new HashMap<>();
	private Map<Integer,ObjectInputStream> inputStreamByClientId = new HashMap<>();
	private Map<Integer, Thread> clientByClientId = new HashMap<>();

	private Integer portNumber = 7070;

	public void run(){
		try(ServerSocket serverSocket = new ServerSocket(portNumber);) {
			while(!Thread.interrupted()){
				try{
					Socket clientSocket = serverSocket.accept();
					ObjectOutputStream clientOut = new ObjectOutputStream(clientSocket.getOutputStream());
					ObjectInputStream clientIn = new ObjectInputStream(clientSocket.getInputStream());
					Integer clientId = clientIn.readInt();
					socketByClientId.put(clientId, clientSocket);
					outputStreamByClientId.put(clientId, clientOut);
					inputStreamByClientId.put(clientId, clientIn);
					Thread client = new ClientThread(clientId, clientSocket, clientOut, clientIn, this);
					clientByClientId.put(clientId, client);
					client.start();
				}catch(Exception e){
					e.printStackTrace();
					Thread.currentThread().interrupt();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void cleanResources(Integer clientId){
		socketByClientId.remove(clientId);
		outputStreamByClientId.remove(clientId);
		inputStreamByClientId.remove(clientId);
		clientByClientId.remove(clientId);
	}
	
	private class ClientThread extends Thread {
		private Socket socket;
		private ObjectOutputStream out;
		private ObjectInputStream in;
		private Integer id;
		private Server adminthread;

		public ClientThread(Integer id, Socket socket, ObjectOutputStream oos, ObjectInputStream ois, Server adminThread){
			this.socket = socket;
			this.out = oos;
			this.in = ois;
			this.id = id;
			this.adminthread = adminThread;
		}

		public void run(){
			try{
				while (!Thread.interrupted()){
					Notification notif = (Notification)in.readObject();
					if (Notification.CLOSE_TYPE.equals(notif.getType())){
						this.in.close();
						this.out.close();
						this.socket.close();
						adminthread.cleanResources(this.id);
						Thread.currentThread().interrupt();
						System.out.println("Client "+id+ " stopped");
					}else{
						System.out.println("received notif: "+notif);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
	}
	
	public static void main (String[] args){
		Server server = new Server();
		server.start();
	}


}
