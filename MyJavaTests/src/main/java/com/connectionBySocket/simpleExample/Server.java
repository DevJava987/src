package com.connectionBySocket.simpleExample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main (String[] args){
		
		if (args.length != 1) {
            System.err.println("Usage: java Server <port number>");
            System.exit(1);
        }
		int portNumber = Integer.parseInt(args[0]);
//		int portNumber = 6969;
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String inputClient = null;
			while ( (inputClient = in.readLine()) != null ){
				System.out.println("SERVER: "+ "Received msg from client - "+inputClient);
				//sending back the msg to client
				out.println(inputClient);
				out.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if (serverSocket!=null){
					serverSocket.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
