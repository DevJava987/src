package com.thread.semaphores.deadlocksAndTryAcquire;

import java.util.concurrent.Semaphore;

public class TryAcquireDemo {

	public static void main(String[] args) throws Exception {
		// nb of permits is set to 1 for explanation needs
		Semaphore s1 = new Semaphore(1);
		// nb of permits is set to 1 for explanation needs
		Semaphore s2 = new Semaphore(1);

		Thread t1 = new Thread(new DoubleResourceGrabber(s1, s2));
		// now reverse them ... here comes trouble!
		Thread t2 = new Thread(new DoubleResourceGrabber(s2, s1));

		t1.start();
		t2.start();

		t1.join();
		t2.join();
		System.out.println("We got lucky!");
	}

	private static class DoubleResourceGrabber implements Runnable {
		private Semaphore first;
		private Semaphore second;

		public DoubleResourceGrabber(Semaphore s1, Semaphore s2) {
			first = s1;
			second = s2;
		}

		private void acquirePermits() {
			boolean firstPermit = false;
			boolean secondPermit = false;
			while (true) {
				firstPermit = first.tryAcquire();
				secondPermit = second.tryAcquire();
				if (firstPermit && secondPermit) {
					Thread t = Thread.currentThread();
					System.out.println(t + " acquired " + first);
					System.out.println(t + " acquired " + second);
					return;
				}
				if (firstPermit) {
					first.release();
				}
				if (secondPermit) {
					second.release();
				}
			}
		}

		public void run() {
			Thread t = Thread.currentThread();
			try {
				// To avoid potential deadlock, we should
				// call the method acquirePermits.
				//acquirePermits();

				first.acquire();
				System.out.println(t + " acquired " + first);
				Thread.sleep(200); // demonstrate deadlock
				second.acquire();
				System.out.println(t + " acquired " + second);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}finally{
				// The release mthod should always called
				// with the finally block
				second.release();
				System.out.println(t + " released " + second);
				first.release();
				System.out.println(t + " released " + first);				
			}
		}
	}

	
}
