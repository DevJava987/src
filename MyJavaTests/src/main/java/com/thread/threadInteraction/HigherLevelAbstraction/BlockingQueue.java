package com.thread.threadInteraction.HigherLevelAbstraction;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * ### Condition.await() ###: How does it work?
 * Causes the current thread to wait until it is signalled or interrupted. 
 * The lock associated with this Condition is atomically released and
 * the current thread becomes disabled for thread scheduling purposes 
 * and lies dormant until one of four things happens: 
 * 1- Some other thread invokes the signal method for this Condition and 
 *    the current thread happens to be chosen as the thread to be awakened; or 
 * 2- Some other thread invokes the signalAll method for this Condition; or 
 * 3- Some other thread interrupts the current thread, and interruption of
 *    thread suspension is supported; or 
 * 4- A "spurious wakeup" occurs. 

 * ### Condition.signal()/signalAll() ###
 * Wakes up one waiting thread. 
 * If any threads are waiting on this condition then one is selected for waking up.
 * That thread must then re-acquire the lock before returning from await. 

 * --!!! Implementation Considerations for both signal() and await() methods!!!-- 
 * The current thread is assumed to hold the lock associated with this Condition
 * when this method is called. It is up to the implementation to determine
 * if this is the case and if not, how to respond. Typically, an exception will be thrown
 * (such as IllegalMonitorStateException) and the implementation must document that fact. 
 */

public class BlockingQueue<T> {

	private Queue<T> queue = new LinkedList<T>();
	private int capacity;
	
	private Lock lock = new ReentrantLock();
	private Condition notFull;
	private Condition notEmpty;
	

	public BlockingQueue(int capacity) {
		this.capacity = capacity;
		notFull = lock.newCondition();
		notEmpty = lock.newCondition();
	}

	//no need to use synchronized
	//Reentrant lock must be used
	public void put(T element) throws InterruptedException {
		lock.lock();
		try{
			while(queue.size() == capacity) {
				System.out.println("PUT Starting wait: "+element.toString());
				//blocks until the condition will be satisfied (=queue not full)
				//the thread will be informed by signal();
				notFull.await();
				//After a signal(), this thread must regain the lock to resume execution
				System.out.println("PUT Ending wait: "+element.toString());
			}
			queue.add(element);
			notEmpty.signal(); // signalAll() for multiple producer/consumer threads
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			lock.unlock();
		}
	}

	public T take() throws InterruptedException {
		lock.lock();
		try{
			while(queue.isEmpty()) {
				System.out.println("TAKE Starting wait");
				//blocks until the queue will be "not empty"
				//until the signal() will be called on this condition
				notEmpty.await();
				System.out.println("TAKE ending wait");
			}
			T item = queue.remove();
			//inform the the read waiting on this condition that the queue is not full
			notFull.signal(); // signalAll() for multiple producer/consumer threads
			return item;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
		return null;
	}

}
