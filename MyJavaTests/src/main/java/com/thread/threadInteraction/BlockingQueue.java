package com.thread.threadInteraction;

import java.util.LinkedList;
import java.util.Queue;


/**
 * Firstly, you need to ensure that any calls to wait() or notify() are within
 * a synchronized region of code (with the wait() and notify() calls being
 * synchronized on the same object). The reason for this (other than the standard
 * thread safety concerns) is due to something known as a missed signal.
 *
 * An example of this, is that a thread may call put() when the queue happens
 * to be full, it then checks the condition, sees that the queue is full,
 * however before it can block another thread is scheduled.
 * This second thread then take()'s an element from the queue, and notifies
 * the waiting threads that the queue is no longer full. Because the first thread
 * has already checked the condition however, it will simply call wait() after being
 * re-scheduled, even though it could make progress.
 * By synchronizing on a shared object, you can ensure that this problem does not occur,
 * as the second thread's take() call will not be able to make progress until the first
 * thread has actually blocked.

 * Secondly, you need to put the condition you are checking in a while loop,
 * rather than an if statement, due to a problem known as spurious wake-ups. 
 * This is where a waiting thread can sometimes be re-activated without notify()
 * being called. Putting this check in a while loop will ensure that 
 * if a spurious wake-up occurs (r�veil fallacieux), the condition will be re-checked,
 * and the thread will call wait() again.

 * @param <T>
 */
public class BlockingQueue<T> {

    private Queue<T> queue = new LinkedList<T>();
    private int capacity;

    public BlockingQueue(int capacity) {
        this.capacity = capacity;
    }

    public synchronized void put(T element) throws InterruptedException {
        while(queue.size() == capacity) {
        	System.out.println("PUT Starting wait: "+element.toString());
        	//Keep in mind that wait() releases the object monitor acquired
        	//by synchronized. Thus other threads can acquire that lock
            wait();
            //After a notify(), this thread must regain the lock to resume execution
        	System.out.println("PUT Ending wait: "+element.toString());
        }
        queue.add(element);
        notify(); // notifyAll() for multiple producer/consumer threads
    }

    public synchronized T take() throws InterruptedException {
        while(queue.isEmpty()) {
        	System.out.println("TAKE Starting wait");
            wait();
        	System.out.println("TAKE ending wait");
        }
        T item = queue.remove();
        notify(); // notifyAll() for multiple producer/consumer threads
        return item;
    }
}