package com.thread.CallableAndFuture;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * The Callable interface is similar to Runnable, in that both are designed
 * for classes whose instances are potentially executed by another thread.
 * A Callable, however, does: 
 * 1- return a result, retrieve the Future and the call the get() method on it. 
 * 2- can throw a checked exception.
 * 
 * Runnable has been around since Java 1.0, but Callable was only introduced in Java 1.5
 * in order to handle use-cases that Runnable does not support. 
 * In theory, the Java team could have changed the signature of the Runnable.run() method,
 * but this would have broken binary compatiblity with pre-1.5 code, requiring recoding
 * when migrating old Java code to newer JVMs. That is a BIG NO-NO.
 * Java strives to be backwards compatible ... and that's been one of Java's biggest selling
 * points for business computing.
 * 
 * 
    - A Callable needs to implement call() method while a Runnable needs to implement run() method.
    - A Callable can return a value but a Runnable cannot.
    - A Callable can throw checked exception but a Runnable cannot.
    - A Callable can be used with ExecutorService#invokeXXX methods but a Runnable cannot be.

    public interface Runnable {
        void run();
    }

    public interface Callable<V> {
        V call() throws Exception;
    }
 * 
 * 
 */

public class CallableAndFutureDemo {

	class Calculator implements Callable<Integer> {
		@Override
		public Integer call() throws Exception {
			System.out.println("starting calculation...");
			Thread.sleep(10000);
			System.out.println("ending calculation...");
			if (false)throw new Exception("Exception occured in the callable...");
			return new Integer(99);
		}
	}

	public static void main(String[] args) {

		// Launching calculation in a separate thread.
		ExecutorService threadPool = Executors.newCachedThreadPool();
		Future<Integer> future = threadPool.submit(new CallableAndFutureDemo().new Calculator());

		// Simulating work here while calculation
		// is being achieved in a separate thread  
		System.out.println("starting main work...");
		try {Thread.sleep(500);} catch (InterruptedException ie) {}
		System.out.println("ending main work...");

		// getting calculation result
		try {
			System.out.println("Before getting result from Future");
			//the Future.get() methods waits for the thread to complete, even if it hasn't started
//			Integer result = future.get(5,TimeUnit.SECONDS);
			Integer result = future.get();
			System.out.println("End of getting result from Future");
			System.out.println("the result of calculationis: " + result);
//		}catch (TimeoutException outExc) {   
//			System.out.println("time out exception");
//			try {Thread.sleep(5000);} catch (InterruptedException ie) {}
//			try{System.out.println("the result after timout: "+future.get());}catch(Exception e){};
//	
//			//If we want to interrupt the executor
//			threadPool.shutdownNow();
//			return;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		finally{
			threadPool.shutdown();
		}
	}
}
