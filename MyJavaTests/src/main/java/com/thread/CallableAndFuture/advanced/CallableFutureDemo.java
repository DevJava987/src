package com.thread.CallableAndFuture.advanced;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableFutureDemo {

	private static int THREADS_NB = 5;
	
	public static void main (String[] args){
		
		ExecutorService executorService = Executors.newFixedThreadPool(THREADS_NB);
		List<Future<Integer>> futures = new ArrayList<>();
		
		//launch all the tasks in parallel way
		for (int i = 0; i < 10; i++) {
			futures.add (executorService.submit(new MyCallable()));
		}
		//do some work here while callables are being executed 
		
		int sum = 0;
		for (Iterator<Future<Integer>> iterator = futures.iterator(); iterator.hasNext();) {
			Future<Integer> future = iterator.next();
			try {
				sum += future.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("the result is: "+sum);
		executorService.shutdown();
	}
	
}
