package com.thread.CallableAndFuture.advanced;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<Integer> {

	@Override
	public Integer call() throws Exception{
		System.out.println("starting calculation...");
		Thread.sleep(10000);
		System.out.println("ending calculation...");
		return new Integer(10);
	}
	
}
