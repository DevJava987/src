package com.thread.cascadingLocking;

import java.util.concurrent.locks.ReentrantLock;

public class Child {

	private int property = 0;
	
	private ReentrantLock lock = new ReentrantLock();
	
	
	public void process(){
		lock.lock();
		
		try{
			//do some processings on the child
			property = property +1;
			Thread.sleep(12000);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
	
}
