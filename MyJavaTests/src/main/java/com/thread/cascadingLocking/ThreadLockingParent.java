package com.thread.cascadingLocking;

public class ThreadLockingParent implements Runnable {

	private Parent parent;
	
	public ThreadLockingParent(Parent parentP){
		this.parent = parentP;
	}
	
	public void run() {
		parent.process();
	}

}
