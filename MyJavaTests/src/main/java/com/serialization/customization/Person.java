package com.serialization.customization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;



/**
 * Please note that the interface java.io.Serializable 
 * has nor attributes neither method declaration.
 * It is used only to identify classes that can be serialized
 *
 */
public class Person implements Serializable {

	private static final long serialVersionUID = 1L;

	private String firstName;
	private String name;
	private int age;

	public Person(String firstName, String name, int age) {
		this.firstName = firstName;
		this.name = name;
		this.age = age;
	}

	/**
	 * This method will be called automatically during serialization
	 * thanks to java reflection. You only have to respect the signature
	 * @param oos
	 * @throws IOException
	 */
	private void writeObject(ObjectOutputStream oos) throws IOException{
		//the default serialization
//		oos.defaultWriteObject();
		
		//Here is how to serialize the object field by field
		oos.writeObject(firstName);
		oos.writeObject(name);
		oos.writeInt(age);
		
		//Do more work here
		System.out.println("A customized serialization...");
	}

	/**
	 * This method will be called automatically during de-serialization
	 * thanks to java reflection. You only have to respect the signature
	 * @param ois
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException{
		//default deserialization
//		ois.defaultReadObject();

		//Here is how to de-serialize the object field by field
		//The fields should be deserialized in the same order
		//as their serialization otherwise a java.io.OptonalDataException
		// will be thrown at runtime
		firstName = (String) ois.readObject();
		name = (String) ois.readObject();
		age = ois.readInt();

		//Do more work here
		System.out.println("A customized de-serialization...");
		this.name += "_bis";
		this.firstName += "_bis";
	}
	
	
	public String toString(){
		return this.firstName + " " + this.name + " "+ this.age;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
