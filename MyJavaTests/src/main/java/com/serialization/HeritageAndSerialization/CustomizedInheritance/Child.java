package com.serialization.HeritageAndSerialization.CustomizedInheritance;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Child extends Father implements Serializable{
	
	private static final long serialVersionUID = -3812369971603114113L;

	private String childName;
    private int childId = 0;
    
    public Child(String childName, int childId){
    	this.childId = childId;
    	this.childName = childName;
    	this.fatherName = "valueFromChild";
    }
    
    private void writeObject(ObjectOutputStream oos) throws IOException{
    	oos.writeObject(childName);
    	oos.writeInt(childId);
    	
    	oos.writeObject(super.fatherName);
    }
    
    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException{
    	childName = (String) ois.readObject();
    	childId = ois.readInt();
    	
    	fatherName = (String) ois.readObject();
    }
    
    
    public String toString(){
    	return childName +" "+ childId +" "+ fatherName;
    }
}
