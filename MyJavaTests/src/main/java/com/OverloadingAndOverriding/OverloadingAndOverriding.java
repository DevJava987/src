package com.OverloadingAndOverriding;

public class OverloadingAndOverriding {

	public static void main(String[] args) {
		OverloadingAndOverriding main = new OverloadingAndOverriding();
		main.init();
	}

	public void init(){
		A a = new B();
		a.print(new B());
		((B)a).print(new B());
	}

	class A {
		public void print(A a){
			System.out.println("A");
		}
	}

	class B extends A{
		public void print(B b){
			System.out.println("B");
		}
	}
}

	
