package com.InnerClasses.NonStaticInnerClasses.MethodLocalInnerClass;

public class App {
	
	private int i = 0;
	
	public void myMethod(){
		
		final int var = -99;
		
		class LocalMethodInnerClass{
			private int localVar = var;
			public void myLocalMethod(){
				System.out.println("method local varibale can be accessed only if they are declared final"+var);
				System.out.println("local method inner class can access the global variable: "+i);
			}
		}
		
	}
	

}
