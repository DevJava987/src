package com.sax.advancedsax;

public class Role {

	private String roleName;
	private String startDate;
	private String endDate;
	
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	@Override
	public String toString(){
		 return "{roleName:"+this.roleName+" startDate:"+this.startDate+" endDate:"+this.endDate+"}";
	}
}
