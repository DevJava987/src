package com.sax.advancedsax;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class XMLSaxParser {

	public static void main(String[] args) {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			MySaxHandler handler = new MySaxHandler();
			saxParser.parse(new File("src/sax/employees.xml"), handler);
			//Get Employees list
			List<Employee> empList = handler.getEmpList();
			//print employee information
			for(Employee emp : empList)
				System.out.println(emp);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
