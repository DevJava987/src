package com.sax;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.LocatorImpl;

public class MySaxHandler extends DefaultHandler {
		
		//Used to hold the key (String) of parsed elements 
		Stack<String> elementStack = new Stack<String>();
		
		//Used to hold the parsed objects
		Stack<Object> objectStack = new Stack<Object>();
		
		private String currentElement(){
			return elementStack.peek();
		}
		
		private String parentOfCurrentElement(){
			if (elementStack.size()<2){
				return null;
			}
			return elementStack.get(elementStack.size()-2);
		}

		private Object currentObject(){
			return objectStack.peek();
		}
		
		private Object parentOfCurrentObject(){
			if (objectStack.size()<2){
				return null;
			}
			return objectStack.get(objectStack.size()-2);
		}

	    //List to hold Employees object
	    private List<Employee> empList = null;
	 
	    //getter method for employee list
	    public List<Employee> getEmpList() {
	        return empList;
	    }
	 
	    @Override
	    public void startElement(String uri, String localName, String qName, Attributes attributes)
	            throws SAXException {
	 
	    	this.elementStack.push(qName);
	    	if (qName.equalsIgnoreCase("Employee")) {
	    		//initialize Employee object and set its attributes
	    		Employee emp = new Employee();
	    		String id = attributes.getValue("id");
	    		String nid = attributes.getValue("nid");
	    		emp.setId(Integer.parseInt(id));
	    		emp.setNid(Integer.parseInt(nid));
	    		this.objectStack.push(emp);
	    		if (empList == null){
	    			empList = new ArrayList<Employee>();
	    		}
	        }else if(qName.equalsIgnoreCase("Role")){
	        	Role role = new Role();
	        	this.objectStack.push(role);
	        	//there is no attribute to set
	        } else if (qName.equalsIgnoreCase("name")) {
	        	//nothing to do in our case
	        } else if (qName.equalsIgnoreCase("age")) {
	        	//nothing to do in our case
	        } else if (qName.equalsIgnoreCase("gender")) {
	        	//nothing to do in our case
	        } else if (qName.equalsIgnoreCase("startDate")){
	        	//nothing to do in our case
	        } else if (qName.equalsIgnoreCase("endDate")){
	        	//nothing to do in our case
	        } else if (qName.equalsIgnoreCase("Employees")){
	        	//nothing to do in our case
	        }
	    }
	 
	 
	    @Override
	    public void endElement(String uri, String localName, String qName) throws SAXException {
	    	if (qName.equalsIgnoreCase("Employee")) {
	    		if ("Role".equalsIgnoreCase(parentOfCurrentElement())){
	    			Role role = (Role) parentOfCurrentObject();
	    			role.addEmployee((Employee)currentObject());
	    		}else if ("Employees".equalsIgnoreCase(parentOfCurrentElement())){
	    			//add Employee object to list
	    			empList.add((Employee)currentObject());
	    		}
	    	}else if(qName.equalsIgnoreCase("Role")){
	    		Employee emp = (Employee)parentOfCurrentObject();
	    		emp.addRole((Role)currentObject());
	    	}
	    	if (qName.equalsIgnoreCase("Role") || qName.equalsIgnoreCase("Employee")){
	    		this.objectStack.pop();
	    	}
	    	this.elementStack.pop();
	    }
	 
	    @Override
	    public void characters(char ch[], int start, int length) throws SAXException {
	        if ("age".equalsIgnoreCase(currentElement())) {
	        	//Age is an elt for employee only. Its parent must be an Employee
	        	//unless the xml file is malformed... TODO handle this case
	            ((Employee)currentObject()).setAge(Integer.parseInt(new String(ch, start, length)));
	        } else if ("name".equalsIgnoreCase(currentElement())) {
	        	//BE CAREFULL: name car be a part of the employee and Role elements
	        	if ("Employee".equalsIgnoreCase(parentOfCurrentElement())){
	        		((Employee)currentObject()).setName(new String(ch, start, length));
	        	}else if ("Role".equalsIgnoreCase(parentOfCurrentElement())){
	        		((Role)currentObject()).setName(new String(ch, start, length));
	        	}
	        } else if ("gender".equalsIgnoreCase(currentElement())) {
	        	((Employee)currentObject()).setGender(new String(ch, start, length));
	        }else if ("startDate".equalsIgnoreCase(currentElement())){
	        	((Role)currentObject()).setStartDate(new String (ch, start, length));
	        }else if ("endDate".equalsIgnoreCase(currentElement())){
	        	((Role)currentObject()).setEndDate(new String (ch, start, length));
	        }
	    }
	    
	    /**
         * Evenement envoye au demarrage du parse du flux xml.
         * @throws SAXException en cas de probleme quelconque ne permettant pas de
         * se lancer dans l'analyse du document.
         * @see org.xml.sax.ContentHandler#startDocument()
         */
        public void startDocument() throws SAXException {
                System.out.println("Debut de l'analyse du document");
        }

        /**
         * Evenement envoye a la fin de l'analyse du flux XML.
         * @throws SAXException en cas de probleme quelconque ne permettant pas de
         * considerer l'analyse du document comme etant complete.
         * @see org.xml.sax.ContentHandler#endDocument()
         */
        public void endDocument() throws SAXException {
                System.out.println("Fin de l'analyse du document" );
        }

        /**
         * Debut de traitement dans un espace de nommage.
         * @param prefixe utilise pour cet espace de nommage dans cette partie de l'arborescence.
         * @param URI de l'espace de nommage.
         * @see org.xml.sax.ContentHandler#startPrefixMapping(java.lang.String, java.lang.String)
         */
        public void startPrefixMapping(String prefix, String URI) throws SAXException {
                System.out.println("Traitement de l'espace de nommage : " + URI + ", prefixe choisi : " + prefix);
        }

        /**
         * Fin de traitement de l'espace de nommage.
         * @param prefixe le prefixe choisi a l'ouverture du traitement de l'espace nommage.
         * @see org.xml.sax.ContentHandler#endPrefixMapping(java.lang.String)
         */
        public void endPrefixMapping(String prefix) throws SAXException {
                System.out.println("Fin de traitement de l'espace de nommage : " + prefix);
        }
	    
	    /**
         * Recu chaque fois que des caracteres d'espacement peuvent etre ignores au sens de
         * XML. C�est-a-dire que cet evenement est envoye pour plusieurs espaces se succedant,
         * les tabulations, et les retours chariot se succedant ainsi que toute combinaison de ces
         * trois types d'occurrence.
         * @param ch les caracteres proprement dits.
         * @param start le rang du premier caractere a traiter effectivement.
         * @param end le rang du dernier caractere a traiter effectivement
         * @see org.xml.sax.ContentHandler#ignorableWhitespace(char[], int, int)
         */
        public void ignorableWhitespace(char[] ch, int start, int end) throws SAXException {
                System.out.println("espaces inutiles rencontres : ..." + new String(ch, start, end) +  "...");
        }

        /**
         * Rencontre une instruction de fonctionnement.
         * @param target la cible de l'instruction de fonctionnement.
         * @param data les valeurs associees a cette cible. En general, elle se presente sous la forme 
         * d'une serie de paires nom/valeur.
         * @see org.xml.sax.ContentHandler#processingInstruction(java.lang.String, java.lang.String)
         */
        public void processingInstruction(String target, String data) throws SAXException {
                System.out.println("Instruction de fonctionnement : " + target);
                System.out.println("  dont les arguments sont : " + data);
        }

        /**
         * Recu a chaque fois qu'une balise est evitee dans le traitement a cause d'un
         * probleme non bloque par le parser. Pour ma part je ne pense pas que vous
         * en ayez besoin dans vos traitements.
         * @see org.xml.sax.ContentHandler#skippedEntity(java.lang.String)
         */
        public void skippedEntity(String arg0) throws SAXException {
                // Je ne fais rien, ce qui se passe n'est pas franchement normal.
                // Pour eviter cet evenement, le mieux est quand meme de specifier une DTD pour vos
                // documents XML et de les faire valider par votre parser.              
        }

        /**
        * Definition du locator qui permet a tout moment pendant l'analyse, de localiser
        * le traitement dans le flux. Le locator par defaut indique, par exemple, le numero
        * de ligne et le numero de caractere sur la ligne. Il est la rgement suffisant
        */
        private Locator locator;
	    
        public MySaxHandler(){
        	locator = new LocatorImpl();
        }
	    
	}
