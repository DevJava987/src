package com.generic.genericClass;

public class Entry<KeyType, ValueType> {

	private KeyType key;
	private ValueType value;
	
	public Entry(KeyType key, ValueType value){
		this.key = key;
		this.value = value;
	}
	
	public KeyType getKey(){
		return key;
	}
	
	public ValueType getValue(){
		return value;
	}
	
	public void setKey(KeyType key){
		this.key = key;
	}
	
	public void setValue(ValueType value){
		this.value = value;
	}
	
	
	public String toString(){
		return "(" + key + ", " + value + ")"; 
	}
	
	
	public static void main (String[] args){
		
		Entry<String,String> strEntry = new Entry<String, String>("key", "value");
		System.out.println(strEntry.toString());
		
		//call the generic method
		String result = Helper.myMethod(strEntry);
		
		//the parameter can be specified explicitly
		Helper.<String>myMethod(strEntry);
		
	}
	
	
}

