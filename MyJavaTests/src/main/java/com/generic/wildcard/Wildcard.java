package com.generic.wildcard;

import java.util.ArrayList;
import java.util.List;

public class Wildcard {

	public static void main (String[] args){
		
//		List<Integer> is not a subtype of List<Number>
		List<Integer> ints = new ArrayList<Integer>();
		ints.add(2);
//		List<Number> nums = ints;  //This would valid if List<Integer> were a subtype of List<Number> according to substitution rule.
//		nums.add(new Double(3.14));  
		Integer x=ints.get(1); // now 3.14 is assigned to an Integer variable!
		//==> To solve this we can use wildcard as follow
		List<? extends Number> numsWildCard = ints;//no pb with that
		Integer i = ints.get(0);
		
		//however we can not add elements in wildcard-based definition list
		List<Integer> integers = new ArrayList<Integer>();
		List<? extends Number> numbers = integers;  // it is OK
		
		//assume it possible to add element in such list, so add an integer which is a subtype of Number
//		numbers.add(new Integer(3));
		
		//assume this is possible to add element in such list, so add a number which is a subtype of Number
//		numbers.add(new Double(3.5));
		
		//this will cause a ClassCastException at runtime !!!!!!
		//because the element whose index is "1" is a Double since 
		//we assume that is possible to add element to wildcard-based definition list
		Integer k = integers.get(1);
	}
	
}
