package com.generic;

import java.util.ArrayList;
import java.util.List;

public class App {
	
	public static void main (String[] args){
		
		//This code compiles without error. But we'll get a classCastException at runtime.
		//This king of error is not detectable at compilation time. 
		List list = new ArrayList<>();
		list.add("string");
		Integer value = (Integer)list.get(0);
		
		
		List<String> stringList = new ArrayList<>();
		stringList.add("string");
		//this compilation-time error avoids us runtime exceptions
//		Integer integerValue = stringList.get(0);
		
		//With generics, it is no longer necessary to cast
		String str = stringList.get(0);
	}

}
