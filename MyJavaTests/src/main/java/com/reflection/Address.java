package com.reflection;

public class Address {

	String city = null;
	String street = null;
	
	public Address(String city,String street){
		this.city = city;
		this.street = street;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}
	
	
}
