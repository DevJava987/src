package com.reflection;

public class Person {
	
	private String name = null;
	private int age = -1;
	private Address address = null;
	

	private Person(String name, int age, Address address){
		this.name = name;
		this.age = age;
		this.address = address;
	}

	
	public static Person createPerson(String name, int age, Address address){
		return new Person( name,  age,  address);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	public void saySomething(String something){
		System.out.println(something);
	}
	
	public static void myStaticMethod(){
		
	}
	
	
	private class MyInnberClass{
		
	}
}
