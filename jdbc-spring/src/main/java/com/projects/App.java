package com.projects;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.projects.dao.EmployeeDAO;

public class App {
    public static void main( String[] args ){
    	
    	ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("spring-context.xml");
    	EmployeeDAO jdbcEmployeeDAO = (EmployeeDAO) ctx.getBean("springJdbcEmployeeDAO");
    	
        Employee employee3 = new Employee(456, "Best Employee", 34);
        jdbcEmployeeDAO.insert(employee3);
 
        Employee employee4 = jdbcEmployeeDAO.findById(456);
        System.out.println(employee4);	
		ctx.close();
    }
}
