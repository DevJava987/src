package com.projects.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.projects.Employee;
import com.sun.org.apache.xerces.internal.xs.PSVIProvider;

public class PlainedJDBCEmployeeDAO implements EmployeeDAO {

	private DataSource dataSource;
	
	public void insert(Employee employee) {

		/**
		 * As you can notice we have to manually manage:
		 * 1- The preparedStatement
		 * 2- The connection open
		 * 3- A long "finally bloc" to clean resources
		 *    with nested try/catch blocks...
		 *    
		 * However with jdbcTemplate all these three points are 
		 * managed by spring.   
		 */
		String query = "INSERT INTO EMPLOYEE VALUES (?,?,?)";
		Connection con = null;
		PreparedStatement preparedStmt = null;
		try{
			//1. First get a connection to the database
			con = dataSource.getConnection();
			//2. create the preparedStatement
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setInt(0, employee.getId());
			preparedStmt.setString(1, employee.getName());
			preparedStmt.setInt(2, employee.getAge());
			
			//3. Execute the query
			preparedStmt.execute();
 		}catch(Exception e){
 			e.printStackTrace();
		}finally{
			//4 Clean resources !!!
			if (preparedStmt!=null){
				try {
					preparedStmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con!=null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Employee findById(int empId) {
		String query = "SELECT * FROM EMPLOYEE WHERE ID = ?";
		Connection con = null;
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		try{
			con = dataSource.getConnection();
			pStmt = con.prepareStatement(query);
			pStmt.setInt(0, empId);
			rs = pStmt.executeQuery();
			Employee emp = null;
			if (rs.next()){
				int id = rs.getInt("ID");
				String name = rs.getString("NAME");
				int age = rs.getInt("AGE");
				emp = new Employee (id, name, age);
			}
			return emp;
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}finally{
			if (rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}
			if (pStmt!=null){
				try {
					pStmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con!=null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
}
