package com.projects.dao;

import com.projects.Employee;

public interface EmployeeDAO {

	public void insert(Employee employee);
	public Employee findById(int id);

}
