package com.projects.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import com.projects.Employee;
import com.projects.dao.mapper.EmployeeRowMapper;

public class SpringJDBCEmployeeDAOImpl implements EmployeeDAO {

	private DataSource dataSource;
	
	/**
	 * Instances of the JdbcTemplate class are threadsafe once configured. 
	 * This is important because it means that you can configure a single 
	 * instance of a JdbcTemplate and then safely inject this shared reference 
	 * into multiple DAOs (or repositories). The JdbcTemplate is stateful, 
	 * in that it maintains a reference to a DataSource, but this state 
	 * is not conversational state.

     * A common practice when using the JdbcTemplate class is to configure a 
     * DataSource in your Spring configuration file, and then dependency-inject
     * that shared DataSource bean into your DAO classes; the JdbcTemplate is 
     * created in the setter for the DataSource. This leads to DAOs that look 
     * in part like the following:
	 */
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public void insert(Employee emp) {
		String query = "INSERT INTO EMPLOYEE VALUES (?,?,?)";
		jdbcTemplate.update(query, new Object[]{emp.getId(),emp.getName(),emp.getAge()});
	}

	public Employee findById(int id) {
		String query = "SELECT * FROM EMPLOYEE WHERE ID = ?";
//		return jdbcTemplate.queryForObject(query, new Object[]{id}, new BeanPropertyRowMapper(Employee.class));
		return jdbcTemplate.queryForObject(query, new Object[]{2}, new EmployeeRowMapper());
	}

	/**
	 * How to query a list of objects
	 */
	public Collection<Employee> findAll(){
		String query = "SELECT * EMPLOYEE";
		List<Employee> employees = new ArrayList<>(); 
		List<Map<String,Object>> allEmployees = jdbcTemplate.queryForList(query);
		for (Iterator<Map<String, Object>>  iterator = allEmployees.iterator(); iterator.hasNext();) {
			Map<String, Object> map = iterator.next();
			int id = (Integer)map.get("ID");
			String name = (String)map.get("NAME");
			int age = (Integer)map.get("AGE");
			employees.add(new Employee(id, name, age));
		}
		return employees;
	}
	
	/**
	 * How to query for only one field/value
	 */
	public String findNameById(int id){
		String query = "SELECT NAME FROM EMPLOYEE WHERE ID = ?";
		//we can specify to the template the type of the expected result
		return jdbcTemplate.queryForObject(query, new Object[]{id}, String.class);
	}
	
	/**
	 * How to insert a list of objects
	 */
	public void insertList(final List<Employee>employees){
		String query = "INSERT INTO EMPLOYEE VALUES (?,?,?)";
		jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter(){

			@Override
			public int getBatchSize() {
				return employees.size();
			}
			@Override
			public void setValues(PreparedStatement pStmt, int i) throws SQLException {
				Employee emp = employees.get(i);
				pStmt.setInt(0, emp.getId());
				pStmt.setString(1, emp.getName());
				pStmt.setInt(2,emp.getAge());
			}
		});
	}
}
