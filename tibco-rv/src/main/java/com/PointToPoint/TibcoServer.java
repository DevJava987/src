package com.PointToPoint;

import com.tibco.tibrv.Tibrv;
import com.tibco.tibrv.TibrvException;
import com.tibco.tibrv.TibrvListener;
import com.tibco.tibrv.TibrvMsg;
import com.tibco.tibrv.TibrvMsgCallback;
import com.tibco.tibrv.TibrvRvdTransport;
import com.tibco.tibrv.TibrvTransport;

public class TibcoServer implements TibrvMsgCallback {
	String service = null;
	String network = null;
	String daemon = null;
	static String inboxServer;
	static String requestSubject = "TIBRV.LOCATE";
	TibrvTransport transport;
	TibrvMsg reply_msg;
	TibrvMsg response_msg;
	int x, y, sum;
	boolean msg_received = true;
	
	public TibcoServer(String args[]){
		try{
			// open Tibrv in native implementation
			Tibrv.open(Tibrv.IMPL_NATIVE);

			// Create RVD transport
			transport = new TibrvRvdTransport(service,network,daemon);
			transport.setDescription("tibrvServer");

			// Create inbox and its listener
			inboxServer = transport.createInbox();
			new TibrvListener(Tibrv.defaultQueue(),	this,transport,inboxServer,null);

			// Create qrequest listener
			new TibrvListener(Tibrv.defaultQueue(),	this,transport,requestSubject,null);

			// create query reply and request response messages
			reply_msg = new TibrvMsg();
			response_msg = new TibrvMsg();
			System.out.println("Server ready...");
			// dispatch Tibrv events with 60 second timeout. If message not
			// received in 60 seconds, quit.
			while(msg_received){
				msg_received = false;
				try{
					Tibrv.defaultQueue().timedDispatch(60);
				}catch (TibrvException e){
					e.printStackTrace();
					System.exit(0);
				}catch(InterruptedException ie){
					System.exit(0);
				}
			}
		}catch(TibrvException e){
			e.printStackTrace();
		}
	}
	
	public void onMsg(TibrvListener listener, TibrvMsg msg){
		msg_received = true;
		if (listener.getSubject() .equals (requestSubject)){
			try{
				reply_msg.setReplySubject(inboxServer);
				transport.sendReply(reply_msg,msg);
			}catch (TibrvException e){
				e.printStackTrace();
				System.exit(0);
			}
		}else {//msg received on the "inboxServer" subject
			try{
				x = msg.getAsInt("x",0);
				y = msg.getAsInt("y",0);
			}catch (TibrvException e){
				return;
			}
			sum = x + y;
			try{
				response_msg.update("sum", sum, TibrvMsg.U32);
				//call extracts the reply subject of an inbound request message,
				//and sends an outbound reply message to that subject
				//#### Give special attention to the order of the arguments ####
				//#### to this method. Reversing the inbound and outbound messages can cause an
				//#### infinite loop, in which the program repeatedly resends the inbound 
				//#### message to itself (and all other recipients).
				transport.sendReply(response_msg, msg);
			}catch (TibrvException e){
				e.printStackTrace();
				return;
			}
		}
	}

	public static void main(String args[]){
		new TibcoServer(args);
	}
}