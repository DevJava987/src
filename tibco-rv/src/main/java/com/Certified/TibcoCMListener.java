package com.Certified;

import com.tibco.tibrv.Tibrv;
import com.tibco.tibrv.TibrvCmListener;
import com.tibco.tibrv.TibrvCmMsg;
import com.tibco.tibrv.TibrvCmTransport;
import com.tibco.tibrv.TibrvException;
import com.tibco.tibrv.TibrvListener;
import com.tibco.tibrv.TibrvMsg;
import com.tibco.tibrv.TibrvMsgCallback;
import com.tibco.tibrv.TibrvQueue;
import com.tibco.tibrv.TibrvRvdTransport;

public class TibcoCMListener extends Thread implements TibrvMsgCallback{

	private TibrvRvdTransport rvdTransport;
	private TibrvCmTransport  cmTransport;
	private TibrvCmListener cmListener;

	private volatile boolean isListenerUp = false;
	
	private TibrvQueue tibrvQueue;
	
	public TibcoCMListener(String service, String network, String deamon, String cmName, String subject){
		
		try {
			//Open Tibrv in native implementation
			Tibrv.open(Tibrv.IMPL_NATIVE);
			
			//Create RVD transport and then CM transport
			rvdTransport = new TibrvRvdTransport(service, network, deamon);
			cmTransport = new TibrvCmTransport (rvdTransport, cmName, true);
			
			//Create the listener using the default queue
			//Please note that the call Tibrv.open() automatically creates the defaultQueue.
			//defaultQueue is static attribut. Is it shared by all programs running in the same jvm
			cmListener = new TibrvCmListener(Tibrv.defaultQueue(),this/*TibrvMsgCallback*/, cmTransport, subject, null);

			//Set explicit confirmation
            cmListener.setExplicitConfirm();
            
			
	        // It is possible to use a customized queue
//          tibrvQueue = new TibrvQueue();
//            tibrvQueue.setName("CM Queue");
//			cmListener = new TibrvCmListener(tibrvQueue,this/*TibrvMsgCallback*/, cmTransport, subject, null);
            
            //It is possible to consume message in the queue in this way
//			TibrvDispatcher disp = new TibrvDispatcher(tibrvQueue);
//			try {
//				//we get stuck inside join() until Tibrv.close() is called
//				//Please see the method stopCMListener()
//				disp.join();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//				Tibrv.close();
//			}
			
		} catch (TibrvException e) {
			e.printStackTrace();
		} 
	}
	

//	public void stopCMListener(){
//		try {
//			Tibrv.close();
//		} catch (TibrvException e) {
//			e.printStackTrace();
//		}
//	}

	public void stopCMListener(){
		//1- DESTROYING RESOURCES
		cmTransport.destroy();
		//Programs must not destroy the default queue!!!
 		//Tibrv.defaultQueue().destroy();
		tibrvQueue.destroy();
		cmListener.destroy();
		
		//2- STOP THE DISPATCHER THREAD
		isListenerUp = false;
		notifyAll();
		//Do not forget the notify all because the Queue.dispatch() is waiting.
		//Otherwise the thread dispatcher will never stop!
		
		//### Here is another way to stop the listener ###
		//Tibrv.close() will destroy resources in the following order:
		//1- dispatchers 
		//2- Transport
		//3- Queues/queue groups
		//4- Events
		// No need to call notifyAll(), dispatcher.destroy() will unblock the wait()
		// and stops the dispatcher thread 
		try {
			Tibrv.close();
		} catch (TibrvException e) {
			e.printStackTrace();
		}

	}

	
	public void onMsg(TibrvListener arg0, TibrvMsg msg) {

		try{
			// Report we are confirming message
			long seqno = TibrvCmMsg.getSequence(msg);
			// If it was not CM message or very first message
			// we'll get seqno=0. Only confirm if seqno > 0.
			process(msg);
			if (seqno > 0){
				//Explicitly confirm delivery of a certified message
				//This method should be used only when automatic 
				//confirmation is overriden by cmListener.setExplicitConfirm()
				cmListener.confirmMsg(msg);
			}
			
			// if message had the reply subject, send the reply
			if (msg.getReplySubject() != null)
            {
                TibrvMsg reply = new TibrvMsg(msg.getAsBytes());
                
                //This convenience call extracts the reply subject of an 
                //inbound request message, and sends a labeled outbound 
                //reply message to that subject. In addition to the
                //convenience, this call is marginally faster than using 
                //separate calls to extract the subject and send the reply.
                //###!!! Give special attention to the order of the arguments !!!### 
                //to this method. Reversing the inbound and outbound messages
                //can cause an infinite loop, in which the program repeatedly 
                //resends the inbound message to itself (and all other recipients).
                cmTransport.sendReply(reply,msg);
            }
		}catch(TibrvException e){
			e.printStackTrace();
		}
	}
	
	
	//The first manner to consumer/dispatch messages in the queue
	//we�re going to go into a look and just keep calling the TibrvQueue.Dispatch()
	//method to keep messages dispatching or block whilst no messages exist
	public void run(){
        isListenerUp = true;
		while(isListenerUp){
			try {
				//1- If the queue is not empty, then this call dispatches 
				//the event at the head of the queue, and then returns.
				//If the queue is empty, then this call blocks indefinitely
				//while waiting for the queue to receive an event.
				
				//2- The more reliable way to interrupt a JNI event dispatch thread is to destroy the
				//TibrvDispatchable	object that the thread dispatches. When the dispatch call
				//encounters the invalid queue or queue group, it throws a TibrvException
				//in the event dispatch thread	
				Tibrv.defaultQueue().dispatch();
				
				//If the defaultQueue is not used
				//tibrvQueue.dispatch();
			} catch (TibrvException e) {
				e.printStackTrace();
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
	}
	
	
	
	private void process (TibrvMsg tibrvMsg){
		System.out.println(tibrvMsg);
	}
}
