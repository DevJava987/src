package com.NotCertified;

import com.tibco.tibrv.Tibrv;
import com.tibco.tibrv.TibrvDispatcher;
import com.tibco.tibrv.TibrvException;
import com.tibco.tibrv.TibrvListener;
import com.tibco.tibrv.TibrvMsg;
import com.tibco.tibrv.TibrvMsgCallback;
import com.tibco.tibrv.TibrvQueue;
import com.tibco.tibrv.TibrvRvdTransport;

public class TibcoListener implements TibrvMsgCallback{

	private String service;
	private String network;
	private String deamon;
	private String subject;
	
	private TibrvRvdTransport transport;
	private TibrvListener listener;
	private TibrvQueue tibrvQueue;
	private TibrvDispatcher dispatcher; 
	
	public TibcoListener(String service, String network, String deamon, String subject){
		this.service = service;
		this.network = network;
		this.deamon = deamon;
		this.subject = subject;
	}

	public void init(){
		try {
			Tibrv.open(Tibrv.IMPL_NATIVE);
			transport = new TibrvRvdTransport(this.service, this.network, this.deamon);
			tibrvQueue = new TibrvQueue();
			listener = new TibrvListener(tibrvQueue, this, transport, subject, null);
		} catch (TibrvException e) {
			e.printStackTrace();
		}
	}
	
	public void start(){
		
		//Create a dispatcher thread. This constructor immediately starts the thread.
		dispatcher = new TibrvDispatcher(tibrvQueue);
		try {
			dispatcher.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
//	public void stopListener(){
//		try {
//			Tibrv.close();
//		} catch (TibrvException e) {
//			e.printStackTrace();
//		}
//	}

	//In either the JNI (native) implementation or the Java implementation,
	//you can interrupt a dispatcher thread by calling its TibrvDispatcher.destroy()
	//method. Destroying the thread insures prompt thread exit�at the latest, after the
	//return of a program callback method (if one is in progress).
	//We can replace all these calls to destroy methods with Tibrv.close()
	public void stopListener(){
		try {
			dispatcher.destroy();
			transport.destroy();
			listener.destroy();
			tibrvQueue.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void onMsg(TibrvListener listener, TibrvMsg msg) {
		//process the message here
	}

}
