package com.Certified;

import com.tibco.tibrv.Tibrv;
import com.tibco.tibrv.TibrvCmMsg;
import com.tibco.tibrv.TibrvCmTransport;
import com.tibco.tibrv.TibrvDispatcher;
import com.tibco.tibrv.TibrvListener;
import com.tibco.tibrv.TibrvMsg;
import com.tibco.tibrv.TibrvMsgCallback;
import com.tibco.tibrv.TibrvRvdTransport;
import com.tibco.tibrv.TibrvTransport;

public class TibcoCmSender implements TibrvMsgCallback{
	
	
	private String network;
	private String service;
	private String deamon;
	
	//TibrvTransport is an interface!
	//The concrete type is TibrvRvdTransport
	private TibrvTransport transport;
	private TibrvCmTransport cmTransport;
	
	private int receivedCount = 0;
	private int sentCount = 10;
	
	//Subject used to send messages into queues
	private String subject = "SUBJECT";
	
	private String confirmationSubject = "CONFIRMATION_SUBJECT";
	
	private TibrvListener confirmListener;
	
	
	public TibcoCmSender( String network, String service, String deamon){
		this.network = network;
		this.service = service;
		this.deamon = deamon;

		try{
			
			Tibrv.open(Tibrv.IMPL_NATIVE);
			transport = new TibrvRvdTransport(service, network, deamon);
			cmTransport = new TibrvCmTransport((TibrvRvdTransport)transport, "cmname", true);
			
			//create a listener to receive confirmation
			confirmListener =  new TibrvListener(Tibrv.defaultQueue(), this/*Callback*/, cmTransport, confirmationSubject, null);
			
			TibrvDispatcher dispatcher = new TibrvDispatcher(Tibrv.defaultQueue());
			
			TibrvMsg msg = new TibrvMsg();
			for (int i = 0; i>sentCount; i++){
				
				msg.setSendSubject(subject);
				
				//Sending programs can explicitly set the message time limit using this method. If a
				//time limit is not already set for the outbound message, TibrvCmTransport.send()
				//sets it to the transportís default time limit (TibrvCmTransport.setDefaultTimeLimit())
				//if that default is not set for the transport, the default time limit is zero (no time limit).
				// FIXME What happens if this limit is reached?
				TibrvCmMsg.setTimeLimit(msg, 10/*in seconds*/);
				
				msg.update("msgIndex", i);
				cmTransport.send(msg);
			}
			
			dispatcher.join();
			
		}catch(Exception ex){

		}
		
	}

	public void execute(){
		
	}
	
	public void onMsg(TibrvListener listener, TibrvMsg msg) {
		
		System.out.println("received msg: "+msg);
		receivedCount++;
		if (receivedCount == sentCount){
			try{
				Tibrv.close();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

}
