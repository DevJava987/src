package com.PointToPoint;

/*
 * Copyright (c) 1998-2002 TIBCO Software Inc.
 * All rights reserved.
 * TIB/Rendezvous is protected under US Patent No. 5,187,787.
 * For more information, please contact:
 * TIBCO Software Inc., Palo Alto, California, USA
 *
 * @(#)tibrvclient.java 1.7
 */
/*
 * tibrvclient - TIB/Rendezvous client program
 *
 * This program will attempt to contact the server program and then
 * perform a series of tests to determine msg throughput and response
 * times.
 *
 * Optionally the user may specify communication parameters for
 * tibrvTransport_Create. If none are specified, default values
 * are used. For information on default values for these parameters,
 * please see the TIBCO/Rendezvous Concepts manual.
 *
 * The user may specify the number of server requests. If none is
 * specified the default value is 10000.
 *
 */
import java.util.Date;
import java.util.Random;

import com.tibco.tibrv.Tibrv;
import com.tibco.tibrv.TibrvDate;
import com.tibco.tibrv.TibrvDispatcher;
import com.tibco.tibrv.TibrvException;
import com.tibco.tibrv.TibrvListener;
import com.tibco.tibrv.TibrvMsg;
import com.tibco.tibrv.TibrvMsgCallback;
import com.tibco.tibrv.TibrvQueue;
import com.tibco.tibrv.TibrvRvdTransport;
import com.tibco.tibrv.TibrvTransport;


public class TibrvRequester implements TibrvMsgCallback{

	String service = null;
	String network = null;
	String daemon = null;
	long requests = 10;
	static long responses = 0;
	static String requestSubject = "TIBRV.LOCATE"; // To find the server
	static String clientInbox;
	static double requestTimeout = 10.0;
	static double test_timeout = 10.0;
	TibrvTransport transport;
	static TibrvDate start_dt;
	static TibrvDate stop_dt;
	static double start_time;
	static double stop_time;
	double elapsed;
	long x = 1;
	long y = 2;
	public TibrvRequester(String args[]){
		
		try{
			// open Tibrv in native implementation
			Tibrv.open(Tibrv.IMPL_NATIVE);
			
			// Create RVD transport
			transport = new TibrvRvdTransport(service,network,daemon);
			transport.setDescription("TibrvRequester");

			// Create a response queue
			TibrvQueue inboxQueue = new TibrvQueue();
			
			// Create an inbox subject for communication with the server
			clientInbox = transport.createInbox();
			
			// Create a listener for this response subject (=inbox).
			// The server will answer the client(requester) on this subject
			new TibrvListener(inboxQueue, this, transport, clientInbox, null);
			
			// Create a message for the query.
			TibrvMsg requestMsg = new TibrvMsg();
			requestMsg.setSendSubject(requestSubject);
			
			//sendRequest generates an inbox reply subject.
			//Send a request message and wait for a reply. This call blocks all other activity on
			//its program thread. When the method receives a reply, it returns the reply. 
			//When the call does not receive a reply, it returns null, indicating timeout
			//Note that there is no need to specify the clientInbox. sendRequest will manage
			//to get the answer from server. The answer will contain the inboxServer
			TibrvMsg replyFromServer = transport.sendRequest(requestMsg, requestTimeout);
			
			// If timeout, reply message is null and query failed.
			if (replyFromServer == null){
				System.err.println("Failed to detect server.");
				System.exit(0);
			}
			
			String serverInbox = replyFromServer.getReplySubject();
			
			// Create a dispatcher with 5 second timeout to process server replies
			TibrvDispatcher dispatcher = new TibrvDispatcher("Dispatcher",inboxQueue,5.0);

			// At this stage the requester knows the server inbox and its own inbox
			// From now on, both or them will communicate through these inboxes
			TibrvMsg msgToSendToServer = new TibrvMsg();
			msgToSendToServer.setSendSubject(serverInbox);
			msgToSendToServer.setReplySubject(clientInbox);

			// Send the specified number of requests to the server.
			Random rand = new Random();
			start_dt = new TibrvDate(new Date());
			start_time = start_dt.getTimeSeconds() + start_dt.getTimeNanoseconds()/1000000000.0;
			for (int i=0; i<requests; i++){
				msgToSendToServer.updateU32("x", (int) rand.nextInt());
				msgToSendToServer.updateU32("y", (int) rand.nextInt());
				transport.send(msgToSendToServer);
			}

		}catch (TibrvException e){
			e.printStackTrace();
			System.exit(0);
		}
	}
	// Listener callback counts responses, reports after all replies received.
	public void onMsg(TibrvListener listener, TibrvMsg msg)	{
		if (++responses >= requests){
			stop_dt = new TibrvDate(new Date());
			stop_time = stop_dt.getTimeSeconds() + stop_dt.getTimeNanoseconds()/1000000000.0;
			elapsed = stop_time - start_time;
			System.out.println("Client received all "+requests+" responses");
			System.out.println(requests+" requests took "+elapsed+" secs to process.");
			System.out.println("Effective rate of "+(requests/elapsed)+" request/sec.");
			transport.destroy();
			System.exit(1);
		}
	}

}	


