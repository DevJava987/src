package com.NotCertified;

import com.tibco.tibrv.Tibrv;
import com.tibco.tibrv.TibrvException;
import com.tibco.tibrv.TibrvMsg;
import com.tibco.tibrv.TibrvMsgField;
import com.tibco.tibrv.TibrvRvdTransport;


/**
 * This class has no callback method because it listen to no thing.
 * It just sends/publishes/ brodcasts messages on a specific subject
 *
 */
public class TibcoSender {

	private String service;
	private String network;
	private String deamon;
	private String subject;
	
	private TibrvRvdTransport transport;
	
	public  TibcoSender (String service, String network, String deamon, String subject){
		this.service = service;
		this.network = network;
		this.deamon = deamon;
		this.subject = subject;
		
		try {
			Tibrv.open(Tibrv.IMPL_NATIVE);
			transport = new TibrvRvdTransport(this.service, this.network, this.deamon);
		} catch (TibrvException e) {
			e.printStackTrace();
		}
	}
	
	public void sendMessage(TibrvMsg msg){
		try {
			//here we are broadcasting messages on the subject "subject".
			//consumers should listen to "subject" to get these messages
			msg.setSendSubject(subject);
			transport.send(msg);
		} catch (TibrvException e) {
			e.printStackTrace();
		}
	}
	
	public void shutDownSender(){
		try {
			//Stop and destroy Rendezvous internal machinery
			//1- Events arrive no longer to the queue
			//2- The transports are unusable. So, no msg can be sent.
			//3- All queues are unusable. No dispatch anymore
			Tibrv.close();
		} catch (TibrvException e) {
			e.printStackTrace();
		}
	}
	
	public static void main (String... args){

		TibcoSender sender = new TibcoSender("service", "network", "deamon", "subject");
		try{
			TibrvMsg msg = new TibrvMsg();
			msg.add("key", "value");

			TibrvMsgField field = new TibrvMsgField("anotherKey","anotherValue");
			msg.addField(field);
			
			sender.sendMessage(msg);
			
		}catch(Exception e){
			e.printStackTrace();
		}

		
	}
	
}
