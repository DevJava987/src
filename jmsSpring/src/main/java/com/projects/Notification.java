package com.projects;

import java.io.Serializable;

public class Notification implements Serializable{

	private static final long serialVersionUID = 8328963412308489233L;
	private String msg;
	
	public Notification(String str){
		msg = str;
	}
	
	
	public String toString(){
		return msg;
	}
}
