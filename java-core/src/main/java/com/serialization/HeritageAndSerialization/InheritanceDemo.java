package com.serialization.HeritageAndSerialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class InheritanceDemo {

	static public void main(String[] args) {
		Child c = new Child("John", 29);
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		try {
			fos = new FileOutputStream("child");
			oos = new ObjectOutputStream(fos);
			oos.writeObject(c);
			oos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (Exception e) {
					oos = null;
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e) {
					fos = null;
				}
			}
		}
		
		//Deserialization
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try{
		fis = new FileInputStream("child");
		ois = new ObjectInputStream(fis);
		Child c_bis = (Child)ois.readObject();
		System.out.println(c_bis);
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			if (ois != null) {
				try {
					ois.close();
				} catch (Exception e) {
					ois = null;
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e) {
					fis = null;
				}
			}
		}
	}
}
