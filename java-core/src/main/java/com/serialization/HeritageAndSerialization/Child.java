package com.serialization.HeritageAndSerialization;

import java.io.Serializable;


/**
 * this class is serializable and inherits from 
 * a NON serializable class Father!!
 *
 */
public class Child extends Father implements Serializable {

	static final private long serialVersionUID = 2549L;
	
	private int age;
	
	Child (String name, int age){
		super(name);
		this.age = age;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public String toString(){
		return this.getName() + " " + this.getAge();
	}
}
