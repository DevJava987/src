package com.serialization.HeritageAndSerialization;


/**
 * this class is NOT serializable.
 * It should have a no-args constructor otherwise an InvalidClassException
 * will be thrown when deserializing its Child class
 * Please note that if both Father and Child classes are serializable,
 * there is no issue to deal with
 *
 */

public class Father {
	
	private String name = null;

	Father(){
		this("Nothing");
	}
	
	Father (String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String toString(){
		return this.name;
	}

}
