package com.serialization.HeritageAndSerialization.CustomizedInheritance;

public class Father {

	protected String fatherName = "defaultFather";
	private int fatherId = 0;
	
	public Father(){
	}
	
	public Father(String fatherName, int fatherId){
		this.fatherId = fatherId;
		this.fatherName = fatherName;
	}
	
	
}
