package com.serialization.simpleExample;

import java.io.Serializable;
/**
 * In order to be serializable, the order should implements
 * the marker interface Serializable and all its filed should be serializable.
 * If one field is not serializable, java.io.NotSerializableException will be thrown 
 * at runtime exception. it is not a checked exception!!!
 *
 */
public class OuterObject implements Serializable{

	private static final long serialVersionUID = 3845592203454694601L;

	private String field1;
	private Integer field2;
	private NestedObject obj;
	
	public OuterObject(String field1, Integer field2, NestedObject obj){
		this.field1 = field1;
		this.field2 = field2;
		this.obj = obj;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public Integer getField2() {
		return field2;
	}

	public void setField2(Integer field2) {
		this.field2 = field2;
	}

	public NestedObject getObj() {
		return obj;
	}

	public void setObj(NestedObject obj) {
		this.obj = obj;
	}
	
}
