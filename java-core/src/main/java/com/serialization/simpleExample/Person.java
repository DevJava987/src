package com.serialization.simpleExample;

import java.io.Serializable;

/**
 * Please note that the interface java.io.Serializable 
 * has nor attributes neither method declaration.
 * It is used only to identify classes that can be serialized
 *
 */
public class Person implements Serializable {

	/**
	 * serialVersionUID is a special static variable used by the serialization
	 * and deserialization process, to verify that a local class is compatible
	 * with the class used to serialize an object. It's not just a static
	 * variable as others, which are definitely not serialized. When an object
	 * of a class is serialized, the class name and serial version UID are
	 * written to the stream of bytes.
	 * 
	 * When it's deserialized, the JVM checks if the serial version UID read
	 * from the stream of bytes is the same as the one of the local class. If
	 * they're not, it doesn't even try to deserialize the object, because it
	 * knows the classes are incompatible.
	 */
	private static final long serialVersionUID = 1L;

	
	private String firstName;
	private String name;
	private transient int age;

	public Person(String firstName, String name, int age) {
		this.firstName = firstName;
		this.name = name;
		this.age = age;
	}

	public String toString() {
		return this.firstName + " " + this.name + " " + this.age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
