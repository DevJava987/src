package com.serialization.simpleExample;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class OuterNestedSerializationDemo {

	
	public static void main(String[] args) {

		NestedObject nestedObject = new NestedObject(2.5d,"coucou");
		OuterObject outerObject = new OuterObject("Jonh", new Integer (30), nestedObject);

		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream("OuterObject");
			oos = new ObjectOutputStream(fos);
			oos.writeObject(outerObject);
			oos.flush();
			System.out.println("Object OuterObject successfully serialized...");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (Exception e) {
					oos = null;
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e) {
					fos = null;
				}
			}
		}

		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			System.out.println("Trying to deserialize object OuterObject...");
			fis = new FileInputStream("OuterObject");
			ois = new ObjectInputStream(fis);
			OuterObject p_bis = (OuterObject) ois.readObject();
			System.out.println("Object OuterObject successfully de-Serialized...");
			System.out.println(p_bis.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (Exception e) {
					ois = null;
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e) {
					fis = null;
				}
			}
		}
	}
	
}
