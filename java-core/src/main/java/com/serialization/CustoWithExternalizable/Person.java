package com.serialization.CustoWithExternalizable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;



/**
 * Please note that the interface java.io.Serializable 
 * has nor attributes neither method declaration.
 * It only identifies classes that can be serialized
 *
 */
public class Person implements Externalizable {

	private static final long serialVersionUID = 1L;

	private String firstName;
	private String name;
	private int age;

	/**
	 * If the default constructor is not defined
	 * a InvalidCalssException will be thrown!
	 * La premi�re chose � savoir est qu'un objet impl�mentant l'interface Externalizeable
	 * doit poss�der un constructeur par d�faut public. En effet, lors de la d�s�rialisation
	 * d'un objet impl�mentant l'interface Externalizable, tous les comportements
	 * de construction par d�faut sont appliqu�s si un ou tous les attributs ne sont pas
	 * d�serializ�s.
	 * Autrement dit si un attribut n'est pas s�rializ�/d�serialis� dans les m�thodes
	 * readExternal() et writeExternal() on lui applique la valeur par d�faut qui lui est
	 * appliqu�e dans le constructeur par d�faut!
	 */
	public Person (){
		firstName = "firstName default constructor";
		name = "name default constructor";
		age = -1;
	}
	
	public Person(String firstName, String name, int age) {
		this.firstName = firstName;
		this.name = name;
		this.age = age;
	}

	//Please note that this method is not called
	//the writeExternal method is called in stead!
	private void writeObject(ObjectOutputStream oos) throws IOException{
		//the default serialization
		oos.defaultWriteObject();
		//Do more work here
		System.out.println("A customized serialization by calling writeObject...");
	}

	//Please note that this method is not called
	//the readExternal method is called in stead!
	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException{
		//default deserialization
		ois.defaultReadObject();
		//Do more work here
		System.out.println("A customized de-serialization by calling readObject...");
		this.name += "_bis";
		this.firstName += "_bis";
	}
	
	
	public String toString(){
		return this.firstName + " " + this.name + " "+ this.age;
	}
	
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		System.out.println("Using Externalizable, readExternal...");
		this.age = in.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		System.out.println("Using Externalizable, writeExternal...");
		out.writeInt(this.age);		
	}

	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
