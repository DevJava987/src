package com.serialization.CustoWithExternalizable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class SerializationWithExternlizableDemo {

	public static void main(String[] args) {

		Person p = new Person("John", "SMITH", 29);

		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream("person.extern");
			oos = new ObjectOutputStream(fos);
			oos.writeObject(p);
			oos.flush();
			System.out.println("Object person successfully serialized with Externalizable...");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (Exception e) {
					oos = null;
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e) {
					fos = null;
				}
			}
		}

		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			System.out.println("Trying to deserialize object person with Externalizable...");
			fis = new FileInputStream("person.extern");
			ois = new ObjectInputStream(fis);
			Person p_bis = (Person) ois.readObject();
			System.out.println("Object person successfully de-Serialized...");
			System.out.println(p_bis.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (Exception e) {
					ois = null;
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e) {
					fis = null;
				}
			}
		}
	}
}
