package com.serialization.customization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CustoSerializationDemo {

	public static void main(String ...args) {

		Person p = new Person("John", "SMITH", 29);

		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream("person");
			oos = new ObjectOutputStream(fos);
			oos.writeObject(p);
			oos.flush();
			System.out.println("Object person successfully serialized...");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (Exception e) {
					oos = null;
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e) {
					fos = null;
				}
			}
		}

		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			System.out.println("Trying to deserialize object person...");
			fis = new FileInputStream("person");
			ois = new ObjectInputStream(fis);
			Person p_bis = (Person) ois.readObject();
			System.out.println("Object person successfully de-Serialized...");
			System.out.println(p_bis.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (Exception e) {
					ois = null;
				}
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e) {
					fis = null;
				}
			}
		}
	}
}
