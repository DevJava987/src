package com.designpattern.BehaviouralDesignPatterns.Observer;

import java.util.Observable;

public class Subject extends Observable implements Runnable {


	@Override
	public void run() {
		int i = 0; 		
		while (i<10){
			try{Thread.sleep(1000);}catch(InterruptedException ie){};
            String elementThatHasChanged = "r�gime cr�tois";
            
            /**
             *Marks this Observable object as having been changed;
             *the hasChanged method will now return true.  
             */
            setChanged();
            
            /**
             * If this object has changed, as indicated by the hasChanged method,
             * then notify all of its observers and then call the clearChanged
             * method to indicate that this object has no longer changed. 
             *Each observer has its update method called with two arguments: this observable object and the arg argument. 
			*/
            notifyObservers(elementThatHasChanged);
            i++;
		}
		
	}

	public String getIdendity() {
		return "MyIdentity";
	}
	
}
