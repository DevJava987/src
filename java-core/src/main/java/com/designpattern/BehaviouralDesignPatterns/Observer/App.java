package com.designpattern.BehaviouralDesignPatterns.Observer;

public class App {

	public static void main(String[] args) {

		// create a subject to observe it 
		final Subject subject = new Subject();

		// create an observer
		final Mylisteners listener = new Mylisteners();

		// subscribe the observer to the event source
		subject.addObserver(listener);
//		subject.deleteObserver(listener);
//		subject.countObservers();
//		subject.hasChanged();

		// starts the event thread
		Thread thread = new Thread(subject);
		thread.start();
	}

}
