package com.designpattern.CreationalDesignPatterns.Singleton;

public class MyDoubleCheckedSingleton {

	private static MyDoubleCheckedSingleton instance = null;

	/**
	 * La pr�sence d'un constructeur priv� supprime le constructeur public par d�faut.
     * De plus, seul le singleton peut s'instancier lui m�me.
	 */
	private MyDoubleCheckedSingleton() {
		// do some work here
	}

	
	public static MyDoubleCheckedSingleton getInstance() {
        //Le "Double-Checked Singleton"/"Singleton doublement v�rifi�" 
		//permet d'�viter un appel co�teux � synchronized, 
        //une fois que l'instanciation est faite la premi�re fois.
		if (instance == null) {
			synchronized (MyDoubleCheckedSingleton.class) {
				// re-v�rifier si le singleton n'a pas �t� instanci� 
				// par un autre thread concurrent
				if (instance == null) {
					instance = new MyDoubleCheckedSingleton();
				}
			}
		}
		return instance;
	}

}
