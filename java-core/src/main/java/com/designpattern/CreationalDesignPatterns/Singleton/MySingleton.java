package com.designpattern.CreationalDesignPatterns.Singleton;

public class MySingleton {
	
	
	/**
	 * 1- Should be private. The only way to access it is through
	 * the static method getInstance()
	 * 2- Should be static because it will be used within a static 
	 * method, the method getInstance()
	 */
	private static MySingleton instance = null;
	
	/**
	 * The constructor is declared private to prevent
	 * instanciating the object from outside this class
	 */
	private MySingleton(){
		//do some work here
	}
	
	public static synchronized MySingleton getInstance(){
		if ( instance == null ){
			instance = new MySingleton();
		}
		return instance;
	}
	
}
