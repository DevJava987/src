package com.designpattern.CreationalDesignPatterns.Builder.ConcretExample;

public class AppFixBuilder {

	public static void main (String[] args){
		
		FixMessageBuilder executionbuilder = new ExecutionReportBuilder();
		FixMessageDirector director = new FixMessageDirector();
		
		director.setBuilder(executionbuilder);
		director.construct();
		FixMessage executionFixMsg = director.getBuilder().getFixMessage();
		
		System.out.println(executionFixMsg);

		FixMessageBuilder nosBuilder = new  NewSingleOrderBuilder();
		director.setBuilder(nosBuilder);
		director.construct();
		FixMessage nosFixMsg = director.getBuilder().getFixMessage();
		System.out.println(nosFixMsg);

		
	}
	
	
}
