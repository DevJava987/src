package com.designpattern.CreationalDesignPatterns.Builder;

public class ObjectToBuild {
	
	Object part1 = null;
	Object part2 = null;
	Object part3 = null;

	public void setPart1(Object part1) {
		this.part1 = part1;
	}
	public void setPart2(Object part2) {
		this.part2 = part2;
	}
	public void setPart3(Object part3) {
		this.part3 = part3;
	}

}
