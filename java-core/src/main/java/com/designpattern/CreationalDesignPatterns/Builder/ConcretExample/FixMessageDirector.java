package com.designpattern.CreationalDesignPatterns.Builder.ConcretExample;


public class FixMessageDirector {

	//builder est de type abstrait!
	//Ainsi, on peut r�utiliser le m�me director
	//mais avec un autre builder ( utiliser le setter)
	private FixMessageBuilder builder;
		
	public void construct(){
		builder.createFixMessage();
		builder.buildHeader();
		builder.buildBody();
		builder.buildTail();
	}
	
	public FixMessageBuilder getBuilder() {
		return builder;
	}

	public void setBuilder(FixMessageBuilder builder) {
		this.builder = builder;
	}
}
