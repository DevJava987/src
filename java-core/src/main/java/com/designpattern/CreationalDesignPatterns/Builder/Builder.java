package com.designpattern.CreationalDesignPatterns.Builder;

public abstract class Builder {

	public ObjectToBuild obj;

	public ObjectToBuild getObjectToBuild() {
		return obj;
	}

	public void createNewObjectTobuild() {
		obj = new ObjectToBuild();
	}

	abstract void buildPart1();

	abstract void buildPart2();

	abstract void buildPart3();

}
