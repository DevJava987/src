package com.designpattern.StructuralDesignPatterns.Delegate;

public interface NotificationService {

	public void notifyClients();
	
}
