package com.designpattern.StructuralDesignPatterns.Proxy;

/**
 * 
 * The proxy design pattern allows you to provide an interface to other
 * objects by creating a wrapper class as the proxy. 
 * The wrapper class, which is the proxy, can add additional functionality
 * to the object of interest WITHOUT CHANGING THE OBJECT's CODE!!! 
 * Below are some of the common examples in which the proxy pattern is used,
    - Adding security access to an existing object. 
      The proxy will determine if the client can access the object of interest.
    - Simplifying the API of complex objects. The proxy can provide a simple API
      so that the client code does not have to deal with the complexity of 
      the object of interest.
     - Adding a thread-safe feature to an existing class without changing
       the existing class's code.
 *      
 * In short, the proxy is the object that is being called by the client
 * to access the real object behind the scene.
 */
public class ServiceProxy implements Service{

	private Service serviceImpl;
	
	public ServiceProxy(){
		serviceImpl = new ServiceImpl();
	}

	@Override
	public void runCommand() {
		doExtraFunctionality();
		if (makeControls()){
			serviceImpl.runCommand();
		}
	}
	
	private void doExtraFunctionality() {
	}

	private boolean makeControls(){
		return false;
	}
	
}
