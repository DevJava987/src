package com.designpattern.StructuralDesignPatterns.Proxy;

public class ServiceImpl implements Service{

	public ServiceImpl(){
	}
	
	/**
	 * We will implement a proxy to control access to this command
	 * The proxy will implements the same interface so that it implements
	 * the method runCommand(). In this method it will call ServiceImpl.runCommand()
	 * but with additional control
	 */
	@Override
	public void runCommand(){
		//do something here
	}
	
	
}
