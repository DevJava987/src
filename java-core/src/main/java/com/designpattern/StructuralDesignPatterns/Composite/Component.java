package com.designpattern.StructuralDesignPatterns.Composite;

public interface Component {

	public void doTask();
	
}
