/*
 * Created on 3 ao�t 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.swing.MyFrame;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;

/**
 * @author tropenat_f
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class HighlightableHeaderRenderer extends SortHeaderRenderer {
	private Color highlightColor=null;
    
    public HighlightableHeaderRenderer() {
    	super();
		setHorizontalTextPosition(LEFT);
		setHorizontalAlignment(CENTER);
    }
    
	/***
	 * Updates the component to reflect the current header
	 */
    @Override
	public Component getTableCellRendererComponent(
		JTable table,
		Object value,
		boolean isSelected,
		boolean hasFocus,
		int row,
		int col) {
		
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		// we'll use the modelIndex instead of 'col', since 'col' is the
		// index of the column on screen and 'modelCol' is the index of
		// the column in the model.
		//int modelCol = table.getColumnModel().getColumn(col).getModelIndex();
				
		// set the header font and colors
		if (table != null) {
			JTableHeader header = table.getTableHeader();
			if (header != null) {
				setForeground(header.getForeground());
				
				// if background is not null, we use it
				if(highlightColor!=null) setBackground(highlightColor);
				else setBackground(header.getBackground());
				
				setFont(header.getFont());
			}
		}
		
		// set the text of the header
		String text = (value == null) ? "" : value.toString();
		setText(text);
		
		
		// set the border of the header
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));		
		return this;
	}
	

	/**
	 * 
	 */
	public void setDefaultHighlightColor() {
		this.highlightColor=null;
	}
	/**
	 * @return true if default color is used
	 */
	public boolean isDefaultHighlightColor() {
		return this.highlightColor==null;
	}	
	/**
	 * @return Returns the highlightColor.
	 */
	public Color getHighlightColor() {
		return highlightColor;
	}
	/**
	 * @param highlightColor The highlightColor to set.
	 */
	public void setHighlightColor(Color highlightColor) {
		this.highlightColor = highlightColor;
	}
 
}
