/*
 * Created on 28 juil. 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.swing.MyFrame;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;



/**
 * @author tropenat_f
 *
 *  A RENOMMER 
 */
public class SortFilterSwitchIcon extends MultiLevelSortableArrowIcon {
	
	
//	private final static Image switchableIcon = TestResourceLoader.getImage("switchable.png"); 
	private boolean filtered=false;
	private boolean switchable=false;
	private int xSwitch=9;
	private int xFilter=3;
	
	/**
	 * @param direction
	 */
	public SortFilterSwitchIcon(int direction) {
		this(direction,false);
	}
	
	public SortFilterSwitchIcon(int direction,boolean f) {
		super(direction);
		setFiltered(f);
	}
	
	public SortFilterSwitchIcon(int direction,int level) {
		this(direction,level,false);
	}
	
	public SortFilterSwitchIcon(int direction,int level,boolean f) {
		super(direction,level);
		setFiltered(f);
	}

    @Override
	public int getIconWidth() {
		int res = super.getIconWidth();
		
		if(filtered)
			res += xFilter;
		if(switchable)
			res += xSwitch;
		
		return res;
	}

    @Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		super.paintIcon(c,g,x,y);

		if(switchable) {
//			g.drawImage(switchableIcon, x+super.getIconWidth()-3
//					, 2 , 9 , 9 , null );
		}
		else if(filtered) {
			g.setColor(Color.red);	
			g.drawString("*",x+super.getIconWidth()-3,12);
		}
	}
	
	/**
	 * @return Returns the filtered.
	 */
	public boolean isFiltered() {
		return filtered;
	}
	/**
	 * @param filtered The filtered to set.
	 */
	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}

	/**
	 * @return the switchable
	 */
	public boolean isSwitchable() {
		return switchable;
	}

	/**
	 * @param switchable the switchable to set
	 */
	public void setSwitchable(boolean switchable) {
		this.switchable = switchable;
	}
}
