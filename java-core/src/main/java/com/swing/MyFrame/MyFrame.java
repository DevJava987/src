package com.swing.MyFrame;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

public class MyFrame extends JFrame {

	static public void main(String... args ){
		MyFrame frame = new MyFrame ("My Frame");
		
	}

	
	public MyFrame(String title){
		JFrame frame = new JFrame (title);
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		
        //Create the menu bar.  Make it have a green background.
        JMenuBar greenMenuBar = new JMenuBar();
        greenMenuBar.setOpaque(true);
        greenMenuBar.setBackground(new Color(154, 165, 127));
        greenMenuBar.setPreferredSize(new Dimension(200, 20));

        //Create a yellow label to put in the content pane.
        JLabel yellowLabel = new JLabel();
        yellowLabel.setOpaque(true);
        yellowLabel.setBackground(new Color(248, 213, 131));
        yellowLabel.setPreferredSize(new Dimension(200, 180));

        //Set the menu bar and add the label to the content pane.
        frame.setJMenuBar(greenMenuBar);
  
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
        		new MyJTabbedPane(), new JPanel());
        frame.getContentPane().add(splitPane);
        
      //Calling setVisible(true) makes the frame appear onscreen
        frame.pack();
        frame.setVisible(true);
	}
	
	
}
