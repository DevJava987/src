package com.swing.MyFrame;

import java.awt.Color;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.swing.MyFrame.column.DefaultColumnsDescriptor;
import com.swing.MyFrame.column.PersonColumns;
import com.swing.MyFrame.column.VisibilityTableColumnModel;


public class MyJTable extends JTable {

	
	protected VisibilityTableColumnModel columnModel;
	
	
	
	public MyJTable(String title){
		super();
	}
	
	
	public MyJTable(MyTableModel model){
		super();
		this.setModel(model);// les donn�es fonctionnelles
		columnModel = new VisibilityTableColumnModel(model);//les donn�es des diff�rents colonnes
		setColumnModel(columnModel);
		int[] displayableColumns = model.getColumnDescriptor().getDisplayableColumns();
		int i = displayableColumns.length;
		for(int k=0; k<i;k++){
			this.setColumnVisible(displayableColumns[k], true);
		}
//		TableColumn myColumn = this.getColumnModel().getColumn(2);
//		myColumn.setPreferredWidth(preferredWidth);
//		myColumn.setMinWidth(minWidth);
//		myColumn.setMaxWidth(maxWidth)
//		myColumn.setResizable(isResizable)
//		myColumn.setCellRenderer(new MyStringCellRenderer());
//		this.setSelectionMode(SelectionMode);
//		this.getSelectedColumn()
//		this.getSelectedRow()
//		this.getColumnModel().getColumn(0).setHeaderRenderer(new MyStringCellRenderer());
        SwitchableColumnMouseAdapter mml = new SwitchableColumnMouseAdapter(this, PersonColumns.AGE_SWITCHABLE,new Color(96, 96, 96));
        getTableHeader().addMouseListener(mml);
        getTableHeader().addMouseMotionListener(mml);

		
	}

	public MyJTable(MyTableModel model, RowFilter filter){
		this(model);
//		In the following example code, you explicitly create a sorter object so you can later use it to specify a filter:
		TableRowSorter sorter = new TableRowSorter<MyTableModel>(model);
		this.setRowSorter(sorter);
		sorter.setRowFilter(filter);

	}
	

	
	
	@Override
	public String getToolTipText(MouseEvent e){
		// A developper!
		return "coucou";
	}
	
	
	@Override
	public ListSelectionModel getSelectionModel() {
		return super.getSelectionModel();
	}
	
	
	public VisibilityTableColumnModel getVisibilityColumnModel() {
		return columnModel;
	}
	
	public void setColumnVisible(int columnIdx,boolean visible) {
		setColumnVisible(columnModel.getColumnByModelIndex(columnIdx),visible);
	}
	public void setColumnVisible(TableColumn column,boolean visible) {
		columnModel.setColumnVisible(column,visible);
	}
	public boolean isColumnVisible(int columnIdx) {
	    return columnModel.isColumnVisible(columnModel.getColumnByModelIndex(columnIdx));
	}
	public void setAllColumnsVisible() {
		columnModel.setAllColumnsVisible();
	}
	
	public void setColumnWidth(int columnIdx,int width) {
		columnModel.getColumnByModelIndex(columnIdx).setWidth(width);
	}

    /**
     * Switch a switchable column automatically
     * @param switchableColumn : the switchable column id
     * @param columnToShow : the id of the choosen column to show
     */
    public void switchColumnTo(int switchableColumn, int columnToShow) {
    	DefaultColumnsDescriptor descr = getColumnsDescriptor();
        
        // MaJ du descriptor
        descr.setColumnSwitchId(switchableColumn, columnToShow);
        
        int indexVue = convertColumnIndexToView(switchableColumn);
        // Si la colonne est visible
        if (indexVue > -1) {
            TableColumn column = columnModel.getColumn(indexVue);
            // MaJ du header
            column.setHeaderValue(descr.getChangeableColumnName(columnToShow));
            // MaJ du renderer
            TableCellRenderer renderer = columnModel.getColumnByModelIndex(columnToShow).getCellRenderer();
            if (renderer != null)
                column.setCellRenderer(renderer);
            getTableHeader().repaint();
        }
    }

	public DefaultColumnsDescriptor getColumnsDescriptor() {
		return ((MyTableModel)getModel()).getColumnDescriptor();
	}

	
	   /**
	    * Override JTable method to display names of switchable columns
	    */
	   @SuppressWarnings("unchecked")
	@Override
	   public void addColumn(TableColumn aColumn) {
		   TableModel model = getModel();
		   
		   DefaultColumnsDescriptor defColDesc = null;
		   
		   if ( model instanceof MyTableModel ) {
			   DefaultColumnsDescriptor colDesc = ((MyTableModel)model).getColumnDescriptor();
			   if (colDesc instanceof DefaultColumnsDescriptor)
				   defColDesc = (DefaultColumnsDescriptor)colDesc;
		   }
		   
	       if (aColumn.getHeaderValue() == null) {
		   	    int modelColumn = aColumn.getModelIndex();
		   	    
		   	    String columnName ;
		   	    if (defColDesc != null) {
		   	    	columnName = defColDesc.getChangeableColumnName(modelColumn);
		   	    } else 
		   	    	columnName = getModel().getColumnName(modelColumn);
		   	    
	              aColumn.setHeaderValue(columnName);
	          }
	          (getColumnModel()).addColumn(aColumn);
	   }

    
}
