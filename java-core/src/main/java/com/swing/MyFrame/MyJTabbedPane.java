package com.swing.MyFrame;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class MyJTabbedPane extends JPanel {

	
	private JButton btnGO = null;
	private JButton btnCANCEL = null;
	private boolean stop = true;
	private MyTableModel myModel ;

	
	    public MyJTabbedPane() {
	        super(new GridLayout(1, 1));

	        JTabbedPane tabbedPane = new JTabbedPane();
	        myModel = new MyTableModel();
	        tabbedPane.setTabPlacement(JTabbedPane.BOTTOM);
//	        ImageIcon icon = createImageIcon("images/middle.gif");
	         
	        JComponent panel1 = makeTextPanel(new MyFilter());
	        tabbedPane.addTab("Tab 1", null/*icon*/, panel1,
	                "Does nothing");
	        tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
	         
	        JComponent panel2 = makeTextPanel(null);
	        tabbedPane.addTab("Tab 2", null/*icon*/, panel2,
	                "Does twice as much nothing");
	        tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
	         
	        JComponent panel3 = makeTextPanel(null);
	        tabbedPane.addTab("Tab 3", null/*icon*/, panel3,
	                "Still does nothing");
	        tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
	         
	        //Add the tabbed pane to this panel.
	        tabbedPane.setPreferredSize(new Dimension(310, 100));
	        add(tabbedPane);
	        //The following line enables to use scrolling tabs.
	        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	       
	    }
	     
	    protected JComponent makeTextPanel(MyFilter filter) {
	        JPanel panel = getMyPanel(filter);
	        panel.setLayout(new GridLayout(1, 1));
	        return panel;
	    }
	
//		public JButton getGOButton(){
//			if (btnGO == null){
//				btnGO = new JButton("go");
//				btnGO.addActionListener(new ActionListener(){
//					public void actionPerformed(ActionEvent e){
//						new QueryExecutor().start();
////						try{
////							Thread.sleep(1000*10);
////						}catch(Exception ex){
////							//no thing
////						}
//					}
//				});
//			}
//			return btnGO;
//		}
//
//		public JButton getCANCELButton(){
//			if (btnCANCEL == null){
//				btnCANCEL = new JButton("cancel");
//				btnCANCEL.addActionListener(new ActionListener(){
//					public void actionPerformed(ActionEvent e){
//						stop = true;
//					}
//				});
//			}
//			return btnCANCEL;
//		}

		public JPanel getMyPanel(MyFilter filter){

			JPanel panel = new JPanel();
			JScrollPane scrollPane = new JScrollPane(getMyTable(filter));
			scrollPane.setHorizontalScrollBar(new JScrollBar() );
			scrollPane.setVerticalScrollBar(new JScrollBar() );
			panel.add(scrollPane);
//			panel.add(getGOButton());
//			panel.add(getCANCELButton());
			return panel;
		}
		
		public JTable getMyTable(MyFilter filter){
				MyJTable table = new MyJTable(myModel, filter);
				table.getSelectionModel().addListSelectionListener(new MyListSelectionListener(table));
			return table;
		}
		
		
//		   class QueryExecutor extends Thread {
//		        @Override
//		        public void run() {
//		        	btnGO.setEnabled(false);
//		        	setCursor(new Cursor(Cursor.WAIT_CURSOR));
//		            stop = false;
//		            SwingUtilities.invokeLater(new Runnable() {
//		                public void run() {
//		                	//do staff
//		                	((MyTableModel)getMyTable().getModel()).setData(new Object[][]{});
//		                	getMyTable().updateUI();
//		                }
//		            });
//		            try {
//		            	//traitement m�tier: acc�s � la base entre autre...
//		            	int compt = 0;
//		            	while (!stop && compt<10){
//		            		Thread.sleep(1000);
//		            		compt++;
//		            	}
//		            } catch (Exception ex) {
//		            	//traitement en cas d'erreur
//		            } finally {
//		            	//eventuellement d'autres traitements ici
//		                SwingUtilities.invokeLater(new Runnable(){
//		                    public void run() {
//		                    	//do staff
//		                    	//typiquement mettre � jour l'IHM en affichant
//		                    	//par exempl le r�sulat du traitement m�tier
//		                    	
//		                    	//A la fin il y aurait typiquement un appel
//		                    	//� la m�thode  javax.swing.jpanel.updateUI();
//		                    	((MyTableModel)getMyTable().getModel()).setData(new Object [][]{
//		                    		    {"Kathy", "Smith",
//		                    		     "Snowboarding", new Integer(5), new Boolean(false)},
//		                    		    {"John", "Doe",
//		                    		     "Rowing", new Integer(3), new Boolean(true)},
//		                    		    {"Sue", "Black",
//		                    		     "Knitting", new Integer(2), new Boolean(false)},
//		                    		    {"Jane", "White",
//		                    		     "Speed reading", new Integer(20), new Boolean(true)},
//		                    		    {"Joe", "Brown",
//		                    		     "Pool", new Integer(10), new Boolean(false)}
//		                    		});
//		                    	btnGO.setEnabled(true);
//		                    	setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//		                    	getMyPanel().updateUI();
//		                    }
//		                });
//		                //eventuellement d'autres traitement ici
//		                
//		            }
//		        }
//		    }

//	    /** Returns an ImageIcon, or null if the path was invalid. */
//	    protected static ImageIcon createImageIcon(String path) {
//	        java.net.URL imgURL = TabbedPaneDemo.class.getResource(path);
//	        if (imgURL != null) {
//	            return new ImageIcon(imgURL);
//	        } else {
//	            System.err.println("Couldn't find file: " + path);
//	            return null;
//	        }
//	    }
	
}
