package com.swing.MyFrame.column;



/**
 * @author Casanova
 * return Info needed to construct the model 
 *
 */
public interface IDefaultColumns   {

	public String[] getNamesKeys();
	public int getGroup();
	public   Class<?>[] getTypes();
	public   int[] getDefaultWidth();
	public   Class<?>[] getFilterTypes();
	public   String[] getGetterMethodNames();
	public   String[] getToolTips();
	public   String[] getSetterMethodNames();
	public   boolean[] getDefaultColumnsSortable();
	public   int[][][] getSwitchable();
	public int[] getDISPLAYABLE_COLUMNS();

	
    public static final short NO_COLUMN = -1;
}