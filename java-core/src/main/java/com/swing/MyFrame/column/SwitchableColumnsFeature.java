package com.swing.MyFrame.column;


/**
 * 
 * Provide event handler and feature used to manage switchable columns 
 * behavior.
 * 
 * @author casanova
 *
 */
public class SwitchableColumnsFeature {
	 
	 
 int[] tabColId;
 int activeColIndex;
 protected DefaultColumnsDescriptor columnsDescriptor;
 
 /**
  *   Constructor  
  */
 public   SwitchableColumnsFeature( int[] tabCol , DefaultColumnsDescriptor columnsDescriptor ){ 
	 this.columnsDescriptor = columnsDescriptor;
	 tabColId = tabCol;
	 if ( tabColId.length > 0 )
		 activeColIndex = 0;
 }


 /**
  * 
  */
 public int switchToColId(int col) {
 	
 	int lastId = tabColId[activeColIndex];
 	
 	for(int i=0 ; i< tabColId.length ; i++ ){
 		if( tabColId[i] == col ){
 			activeColIndex = i;
 		}
 	}
 	return lastId;
 	
 }

 /**
  * 
  */
 public int getColId() {
 	
 	return tabColId[activeColIndex];  	
 }


/**
 * 
 * @return int : the id of the new column active 
 */
public int switchNext() {

	if( activeColIndex == tabColId.length -1 ) 
		activeColIndex = 0;
	else
		activeColIndex++;
	
	return tabColId[activeColIndex];
}



}
