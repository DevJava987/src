package com.swing.MyFrame.column;

public class PersonColumns implements IDefaultColumns {
	
	public static final short ID = 0;
	public static final short FIRST_NAME = 1;
	public static final short LAST_NAME = 2;
	public static final short SPORT = 3;
	public static final short AGE_YEAR = 4;
	public static final short AGE_NUMBER = 5;
	public static final short VEGETARIAN = 6;
	public static final short AGE_SWITCHABLE = 7;
	
	
	private final String[] NAME_KEYS = {"Id","First Name","Last Name","Sport","Birth Date","# of Years","Vegetarian", "Age"};
	private final String[] TOOL_TIPS = {"Id","First Name","Last Name","Sport","Birth Date","# of Years","Vegetarian", "Age"};
	private final Class<?>[] TYPES = { Integer.class,String.class,String.class,String.class,String.class,Integer.class,Boolean.class,String.class};
	private final int[] DEFAULT_WIDTH = {60,60,60,60,60,60,60,60};
	private final Class<?>[] FILTER_TYPES = {null,null,null,null,null,null,null,null};
	private final boolean[] DEFAULT_COLUMNS_SORTABLE = {true,true,true,true,true,true,true,true};
	private final int[] DISPLAYABLE_COLUMNS ={0,1,2,3,7,6};
	// first value is the name of the swithable column and 
	// next values are the content of the switchable column
	private int[][][] SWITCHABLE = {{{AGE_SWITCHABLE},{AGE_YEAR,AGE_NUMBER}}};
	
	@Override
	public boolean[] getDefaultColumnsSortable() {
		return DEFAULT_COLUMNS_SORTABLE;
	}

	@Override
	public int[] getDefaultWidth() {
		return DEFAULT_WIDTH;
	}

	@Override
	public Class<?>[] getFilterTypes() {
		return FILTER_TYPES;
	}

	@Override
	public String[] getGetterMethodNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroup() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String[] getNamesKeys() {
		return NAME_KEYS;
	}

	@Override
	public String[] getSetterMethodNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[][][] getSwitchable() {
		return SWITCHABLE;
	}

	@Override
	public String[] getToolTips() {
		return TOOL_TIPS;
	}

	@Override
	public Class<?>[] getTypes() {
		return TYPES;
	}
	
	@Override
	public int[] getDISPLAYABLE_COLUMNS() {
		return DISPLAYABLE_COLUMNS;
	}

}
