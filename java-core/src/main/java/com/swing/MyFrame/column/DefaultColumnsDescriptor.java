package com.swing.MyFrame.column;

import java.util.HashMap;
import java.util.Map;


/**
 * @author tropenat_f
 */
public class DefaultColumnsDescriptor{

	private String[] names;

	private String[] setterMethodNames;
	
	/** Indicates which columns are sortable */
	boolean[] columnsSortable;

	/** Indicates which columns are filtrable */
	boolean[] columnsEnabled;

	/** Indicates which columns are filtrable */
	//boolean[] columnsFiltrable;

	private SwitchableColumnsFeature[] switchFeatures;
	private IDefaultColumns colData;
	
	private int[] displayableColumns;
	
	public DefaultColumnsDescriptor( IDefaultColumns colData , int[] columns) {
		this.colData = colData;
	    int n = colData.getNamesKeys().length; 
	    this.names = new String[n];
	    this.setterMethodNames = new String[n];
	    this.setterMethodNames[colData.getGroup()] = ""; //Colonne du TreeTable - il ne faut pas que cette methode soit null car sinon l'arbre ne sera pas editable

	    this.columnsSortable = colData.getDefaultColumnsSortable();
	    //this.columnsFiltrable = new boolean[n];
	    
	    this.columnsEnabled = new boolean[n];
	    
	    //initialisation des noms
	    for (int i=0;i<n;i++) {
	        names[i] = colData.getNamesKeys()[i];
	    }
	    //initialisation des colonnes utilisees
	    for (int i = 0; i < columns.length; i++) {
	        this.columnsEnabled[columns[i]]=true;
        }
	    
	    displayableColumns = colData.getDISPLAYABLE_COLUMNS();
		switchFeatures = new SwitchableColumnsFeature[n];
	    for (int i=0 ; i < colData.getSwitchable().length ; i++){
	    	//init the switchable column
	    	SwitchableColumnsFeature switchCols = new SwitchableColumnsFeature(colData.getSwitchable()[i][1], this);
	    	switchFeatures[colData.getSwitchable()[i][0][0]] =  switchCols ;
	    }
	    
	}
	

	public int getColumnCount() {
		return colData.getNamesKeys().length;
	}
	
	public String getChangeableColumnName(int col) {
		if ( switchFeatures[col] == null )
			return names[col];
        return  names[switchFeatures[col].getColId()] ;
	}
	
	public String getColumnName(int col) {			
		
		return names[col];
	}

	public int getColumnWidth(int col) {
						
		return colData.getDefaultWidth()[col];
	}

	public String getGetterMethodName(int col) {
		if ( switchFeatures[col] == null )
			return colData.getGetterMethodNames()[col];
        return colData.getGetterMethodNames()[ switchFeatures[col].getColId() ];
	}

	public String getSetterMethodName(int col) {
		if ( switchFeatures[col] == null )
			return setterMethodNames[col];
        return setterMethodNames[switchFeatures[col].getColId()];
	}

	public Class<?> getType(int col) {
		return colData.getTypes()[col];
	}
	
	/**
	 * not working for switchable cols
	 */
	public void setColumnName(int col, String name) {
		names[col]=name;
	}
	/**
	 * Not working for switchable column 
	 */
	public void setSetterMethodName(int col, String method) {
		setterMethodNames[col]=method;
	}
	
	/**
	 * Not working for switchable column 
	 */
	public void setDefaultSetterMethodName(int col) {
		setterMethodNames[col]=colData.getSetterMethodNames()[col];		
	}

	public void setColumnSortable(int col, boolean sortable) {
		columnsSortable[col]=sortable;
	}

	public boolean isColumnSortable(int col) {
		return columnsSortable[col];
	}
	
	public boolean isColumnSwitchable(int col) {
		return ( getSwitchableColumnsFeature(col) != null );
	}

	private SwitchableColumnsFeature getSwitchableColumnsFeature(int col){
		return switchFeatures[col];
	}
	
    /**
     * Return the id of the chosen column in a switchable one.
     * @param col : the id of the switchable column
     */
	public int getColumnSwitchId(int col) { 
		//cherche la Switchable feature et appelle la methode pour savoir la colonne active
		return getSwitchableColumnsFeature(col).getColId();
	}
    
    /**
     * Return a map of columns id (Integer) by switchable column id (Integer)
     * @return Map
     */
    public Map<Integer,Integer> getAllColumnSwitchId() {
        Map<Integer,Integer> toReturn = new HashMap<Integer,Integer>();
        for (int i = 0; i < switchFeatures.length; i++) {
            SwitchableColumnsFeature scf = getSwitchableColumnsFeature(i);
            if (scf != null)
                toReturn.put(new Integer(i), new Integer(scf.getColId()));
        }
        return toReturn;
    }
    
    /**
     * Switch the column to the next one.
     * @param col : the id of the switchable column
     */
	public int setColumnSwitchNext(int col) { 
		//cherche la Switchable feature et appelle la methode pour passer a la colonne suivante
		SwitchableColumnsFeature s = getSwitchableColumnsFeature(col);
		if ( s == null ) {
			System.out.println("The table call a switchable col that isn't.");
			return 0;
		}
        this.setColumnEnabled( s.getColId() , false );
        this.setColumnEnabled( s.switchNext() , true );
        return s.getColId();
	}
 
    /**
     * Switch the switchable column to a precise choice.
     * @param col : the id of the switchable column
     * @param id : the id of the chosen column
     */
	public void setColumnSwitchId(int col,int id) {
		if ( getSwitchableColumnsFeature(col) == null) {
			System.out.println("Impossible to switch col " + col + " to " + id + "in ColumnsDescriptor ");
		}  
		else {
			getSwitchableColumnsFeature(col).switchToColId(id);
		}		
	}
	
    /**
     * Get the first choice of a switchable column.
     * ie the column shown by default.
     * @param col : the id of the switchable column
     */
	public int getSwitchableRoot(int col){
	    for (int i=0 ; i < colData.getSwitchable().length ; i++){
	    	for (int j=0 ; j < colData.getSwitchable()[i][1].length ; j++){
    			if ( colData.getSwitchable()[i][1][j] == col ){
    				return  colData.getSwitchable()[i][0][0];
    			}
	    	}	
	    }
		return col;
	}
	
    public Class<?> getFilterType(int col) {
        return colData.getFilterTypes()[col];
    }
    
    public String getColumnToolTip(int col) {
        return colData.getToolTips()[col];
    }
    
	public boolean isColumnEnabled(int col) {
		return columnsEnabled[col];
	}

	public void setColumnEnabled(int col, boolean enabled) {
		columnsEnabled[col] = enabled;
	}

    /**
     *  
     */
    public boolean isSaved(int col) {
        return true;
    }


	public int[] getDisplayableColumns() {
		return displayableColumns;
	}


	public void setDisplayableColumns(int[] displayableColumns) {
		this.displayableColumns = displayableColumns;
	}

}