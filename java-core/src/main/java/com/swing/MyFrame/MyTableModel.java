package com.swing.MyFrame;

import java.util.HashMap;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import com.swing.MyFrame.column.DefaultColumnsDescriptor;
import com.swing.MyFrame.column.PersonColumns;

public class MyTableModel extends AbstractTableModel {

	   private final static int[] COLUMNS_ENABLED = new int[] {};

	
	protected DefaultColumnsDescriptor columnsDescriptor  = new DefaultColumnsDescriptor(new PersonColumns(),COLUMNS_ENABLED);;
	
	
	private Object[][] data = {
			{new Integer(1),"Kathy", "Smith",
				"Snowboarding", new Integer(5), new Boolean(false)},
				{new Integer(2),"John", "Doe",
					"Rowing", new Integer(3), new Boolean(true)},
					{new Integer(3),"Sue", "Black",
						"Knitting", new Integer(2), new Boolean(false)},
						{new Integer(4),"Jane", "White",
							"Speed reading", new Integer(21), new Boolean(true)},
							{new Integer(5),"Joe", "Brown",
								"Pool", new Integer(25), new Boolean(false)}
	};

	Map<Integer,Person> dataMap = new HashMap<Integer,Person>();
	private String[] columnNames = {"Id","First Name","Last Name","Sport","# of Years","Vegetarian"};

	public MyTableModel(){
		dataMap.put(new Integer(1), new Person(new Integer(1),"Kathy", "Smith","Snowboarding", new Integer(5), new Boolean(false)));
		dataMap.put(new Integer(2), new Person(new Integer(2),"John", "Doe","Rowing", new Integer(3), new Boolean(true)));
		dataMap.put(new Integer(3), new Person(new Integer(3),"Black","Knitting","Speed reading", new Integer(2), new Boolean(false)));
		dataMap.put(new Integer(4), new Person(new Integer(4),"Jane", "White","Speed reading", new Integer(21), new Boolean(true)));
		dataMap.put(new Integer(5), new Person(new Integer(5),"Joe", "Brown","Pool", new Integer(25), new Boolean(false)));
	}

	public int getRowCount() {
		return dataMap.size();
//		return  data.length;
	}


//	public int getColumnCount() {
//		return columnNames.length;
//	}
//
//	public String getColumnName(int col) {
//		return columnNames[col];
//	}
	
	
    @Override
	public int getColumnCount() {
		return this.columnsDescriptor.getColumnCount();
	}

	/**
	 * Returns the column name passed into the constructor.
	 */
    @Override
	public String getColumnName(int column) {
		if (column < 0 || column >= this.columnsDescriptor.getColumnCount())
			return null;
		return this.columnsDescriptor.getColumnName(column);
	}

	/**
	 * Returns the column class for column <code>column</code>. This is set
	 * in the constructor.
	 */
    @Override
	public Class<?> getColumnClass(int column) {
		if (column < 0 || column >= this.columnsDescriptor.getColumnCount()) 
			return null;
		return this.columnsDescriptor.getType(column);
	}
	
//	public Class getColumnClass(int c) {
//		return getValueAt(0, c).getClass();
//	}
//
	
//	public Object getValueAt(int row, int col) {
//		return data[row][col];
//	}
	public Object getValueAt(int row, int col) {
		Person p = (Person)dataMap.get(new Integer(row+1));
		switch (col){
		case PersonColumns.ID:
			return p.getId();
		case PersonColumns.FIRST_NAME:
			return p.getFirstName();
		case PersonColumns.LAST_NAME:
			return p.getLastName();
		case PersonColumns.SPORT:
			return p.getSport();
		case PersonColumns.AGE_SWITCHABLE:
			return p.getAge();
		case PersonColumns.VEGETARIAN:
			return p.isVegetarian();
		}
		return new Integer(22);
	}

	
	/*
	 * Don't need to implement this method unless your table's
	 * editable.
	 */
	public boolean isCellEditable(int row, int col) {
		//Note that the data/cell address is constant,
		//no matter where the cell appears onscreen.
		if (col < 2) {
			return false;
		} else {
			return true;
		}
	}
	/*
	 * Don't need to implement this method unless your table's
	 * data can change.
	 */
	public void setValueAt(Object value, int row, int col) {
		data[row][col] = value;
//		this.fireTableCellUpdated(row, col); //�a ne marche pas
//		this.fireTableRowsDeleted(row, row);// �a marche mais �a ne fait que supprimer la ligne qlq soit la valeur
		this.fireTableDataChanged();
//		this.fireTableStructureChanged();
//		this.fireTableRowsUpdated(row, row);
	}

	public void setData(Object [][] data){
		this.data = data;
	}

	public Object[][] getData(){
		return data;
	}
	public Map<Integer, Person> getDataMap() {
		return dataMap;
	}
	public void setDataMap(Map<Integer, Person> dataMap) {
		this.dataMap = dataMap;
	}

	public DefaultColumnsDescriptor getColumnDescriptor() {
		return columnsDescriptor;
	}

	public void setColumnDescriptor(DefaultColumnsDescriptor columnDescriptor) {
		this.columnsDescriptor = columnDescriptor;
	}
}
