package com.thread.readWriteLocks;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Calculator {
	private int calculatedValue;
	private int value;
	
	/**
	 * In the example below, we can have hundreds of threads
	 * reading the same value at once with no issue, and we
	 * only block readers when we acquire the write lock. 
	 * Remember that: many readers can acquire the read lock 
	 * at the same time, but there are no readers OR writers 
	 * allowed when acquiring the write lock.
	 */
	private ReadWriteLock lock = new ReentrantReadWriteLock();

	private int doMySlowCalculation(int value2) {
		try {
			Thread.sleep(15000);
		} catch (InterruptedException ie) {
		}
		return 0;
	}

	public void calculate(int value) {
		lock.writeLock().lock();
		try {
			this.value = value;
			this.calculatedValue = doMySlowCalculation(value);
		} finally {
			lock.writeLock().unlock();
		}
	}

	public int getCalculatedValue() {
		lock.readLock().lock();
		try {
			return calculatedValue;
		} finally {
			lock.readLock().unlock();
		}
	}

	public int getValue() {
		lock.readLock().lock();
		try {
			return value;
		} finally {
			lock.readLock().unlock();
		}
	}
	
	/**
	 * Here is an equivalent of using of the previous code
	 * but with use of the synchronized keyword.
	 * Simple, but if we have a lot of contention or if we
	 * perform a lot of reads and few writes, synchronization
	 * could hurt performance. Since frequently reads occur
	 * a lot more often than writes, Using a ReadWriteLock
	 *  helps us minimize the issue.
	 */
	
//    public synchronized void calculate(int value) {
//        this.value = value;
//        this.calculatedValue = doMySlowCalculation(value);
//    }
//
//    public synchronized int getCalculatedValue() {
//        return calculatedValue;
//    }
//
//    public synchronized int getValue() {
//        return value;
//    }
}