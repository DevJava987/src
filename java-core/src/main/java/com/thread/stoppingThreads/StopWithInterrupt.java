package com.thread.stoppingThreads;

public class StopWithInterrupt extends Thread{

	
	public void run(){
		int counter = 0;
		while (!this.isInterrupted()){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				this.interrupt();
			}
			System.out.println("I am still alive");
		}
		System.out.println("The thread is interrupted");
	}
	
	
	public static void main (String[] args){
		
		//on cr�e un thread et on va l'interrompre
		//apr�s un sleep de 10 secondes
		Thread t = new StopWithInterrupt();
		t.start();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("We are about to stop the thread...");
		t.interrupt();
	}
	
}
