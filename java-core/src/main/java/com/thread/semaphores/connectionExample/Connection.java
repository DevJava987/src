package com.thread.semaphores.connectionExample;

import java.util.concurrent.Semaphore;

/**
 * A counting semaphore. Conceptually, a semaphore maintains a set of permits. 
 * Each acquire blocks if necessary until a permit is available, and then takes it. 
 * Each release adds a permit, potentially releasing a blocking acquirer.
 * However, no actual permit objects are used; the Semaphore just keeps a count
 * of the number available and acts accordingly. 
 * 
 * Semaphores are often used to restrict the number of threads than can access
 * some (physical or logical) resource. Here, in the example below, the semaphore
 * limits the access to Connection to 10 threads ( there is only 10 permits )
 *
 */
public class Connection {

	private static Connection instance = new Connection();
	private int connections = 0;

	private Semaphore sem = new Semaphore(10);
//	private Semaphore semWithFairness = new Semaphore(10, true);

	private Connection() {

	}

	public static Connection getInstance() {
		return instance;
	}

	public void connect() {
		try {
			try {
				sem.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//Before doing some work acquire connection object
			synchronized (this) {
				connections++;
				System.out.println("Current connections: " + connections);
			}
			
			try {
				Thread.sleep(2000);// Simulating some work using a connection here!!!
			} catch (InterruptedException ie) {}
			
			//After doing some work release the connection object
			synchronized (this) {
				connections--;
			}
			
		} finally {
			sem.release();
		}
	}
}
