package com.thread.semaphores;

import java.util.concurrent.Semaphore;

/**
 * A semaphore is usually used to control access to some resources
 * 
 */

public class BasicExample {

	public static void main(String[] args) throws InterruptedException {
		Semaphore sem = new Semaphore(0);
		
		// Creates a Semaphore with the given number of permits 
		// and the given fairness setting.
		Semaphore semaphoreWithFairness = new Semaphore(15,true);

		//increases the number of permits
		sem.release();
		
		//decreases the number of permits
		sem.acquire();

		// Semaphore.availablePermits() returns the number of available permits
		System.out.println("available permits: " + sem.availablePermits());

		
		/**
		 * the following sample of code is equivalent to
		 * Lock.lock() and Lock.unlock().
		 * Pay attention semaphore.release should be called
		 * within a finally bloc
		 * How to simulate a mutual exclusion by using Semaphore:
		 */
//		Semaphore sem = new Semaphore(1);// only one permits!!!
//		sem.acquire();
//		sem.release();
		
	}

}
