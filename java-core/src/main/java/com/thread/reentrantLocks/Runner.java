package com.thread.reentrantLocks;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {

	private Account acc1 = new Account();
	private Account acc2 = new Account();

	private final Lock lock1 = new ReentrantLock();
	private final Lock lock2 = new ReentrantLock();

	private void acquireLocks() throws InterruptedException {
		while (true){
			boolean gotLock1 = false;
			boolean gotLock2 = false;
			try{
				gotLock1 = lock1.tryLock();
				gotLock2 = lock2.tryLock();
			}finally{
				if (gotLock1 && gotLock2){
					return;
				}
				if (gotLock1){
					lock1.unlock();
				}
				if (gotLock2){
					lock2.unlock();
				}
			}
			Thread.sleep(1);
		}
	}

	private void freeLocks(){
		lock1.unlock();
		lock2.unlock();
	}
	
	public void firstThread() throws InterruptedException {
		Random random = new Random();
		for (int i = 0; i < 10000; i++) {
			lock1.lock();
			lock2.lock();
			try {
				Account.transfer(acc1, acc2, random.nextInt(100));
			} finally {
				lock1.unlock();
				lock2.unlock();
			}
		}
	}

	/**
	 * In order to avoid deadLocking tryLock()
	 * method can be used. The firstThread() method
	 * becomes as follow. Idem for the secondThread method.
	 */
//	public void firstThread() throws InterruptedException {
//		Random random = new Random();
//		for (int i = 0; i < 10000; i++) {
//			acquireLocks();
//			try {
//				Account.transfer(acc1, acc2, random.nextInt(100));
//			} finally {
//				freeLocks();
//			}
//		}
//	}
	
	
	public void secondThread() throws InterruptedException {
		Random random = new Random();
		for (int i = 0; i < 10000; i++) {
			lock2.lock();
			lock1.lock();
			try {
				Account.transfer(acc2, acc1, random.nextInt(100));
			} finally {
				lock2.unlock();
				lock1.unlock();
			}
		}
	}

	
//	public void secondThread() throws InterruptedException {
//		Random random = new Random();
//		for (int i = 0; i < 10000; i++) {
//			acquireLocks();
//			try {
//				Account.transfer(acc2, acc1, random.nextInt(100));
//			} finally {
//				freeLocks();
//			}
//		}
//	}
	
	
	public void finished() {
		System.out.println("Account 1 balance: " + acc1.getBalance());
		System.out.println("Account 2 balance: " + acc2.getBalance());
		System.out.println("Total balance is: "
				+ (acc1.getBalance() + acc2.getBalance()));
	}

}
