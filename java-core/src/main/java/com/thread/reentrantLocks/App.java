package com.thread.reentrantLocks;

public class App {

	public static void main (String[] args) throws Exception{
		
		final Runner runner = new Runner();
		
		Thread t1 =  new Thread( new Runnable() {
			@Override
			public void run() {
				try{
				runner.firstThread();
				}catch (InterruptedException ie){
					ie.printStackTrace();
				}
			}
		});
		
		Thread t2 =  new Thread( new Runnable() {
			@Override
			public void run() {
				try{
				runner.secondThread();
				}catch (InterruptedException ie){
					ie.printStackTrace();
				}
			}
		});

		System.out.println("Starting the 2 threads");
		t1.start();
		t2.start();
		
		//the current thread should wait for the 2 threads to end 
		//in order to display coherent data ( balance of both accounts)
		t1.join();
		t2.join();		
		runner.finished();
	}
}
