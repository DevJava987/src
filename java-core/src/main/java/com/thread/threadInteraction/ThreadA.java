package com.thread.threadInteraction;

public class ThreadA extends Thread {

	public static void main(String[] args) {
		// Particular syntax because we are using an inner class here
		ThreadA.ThreadB b = new ThreadA().new ThreadB();
		b.start();

		// This code snippet is used to make threadB hold
		// the lock before Thread A ( just for test needs)
		// Should be commented if not needed
		try {
			System.out.println("A will be sleeping for 5 seconds...");
			Thread.sleep(5000);
			System.out.println("A ends sleeping...");
		} catch (InterruptedException ie) {
		}

		/**
		 * wait(), notify(), and notifyAll() must be called from 
		 * within a synchronized context!
		 * A thread can�t invoke a wait or notify method on an object
		 * unless it owns that object�s lock.
		 */
		synchronized (b) {
			try {
				System.out.println(" Start waiting for b to complete...");
				if (!b.isAlive()) {
					System.out.println("thread B is NOT alive!!!");
				}
//				 b.wait(5000);
				b.wait();
				System.out.println("Finished to wait for thread B...");
			} catch (InterruptedException e) {
			}
		}
		// lecture des donn�es de B et traitements
		System.out.println("Total is: " + b.total);

	}

	public class ThreadB extends Thread {

		int total;

		public void run() {
			synchronized (this) {
				for (int i = 0; i < 15; i++) {
					total += i;
//					try {
//						Thread.sleep(5000);
						System.out.println(i);
//					} catch (InterruptedException ie) {
						// no thing to do
//					}
				}
				notify();
				System.out.println("Thread B has just notified thread A");
				 try{Thread.sleep(5000);}catch(InterruptedException ie){}
			}
		}

	}

}
