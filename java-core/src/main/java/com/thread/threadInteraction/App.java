package com.thread.threadInteraction;


public class App {

	
	public static void main (String[] args){
		
		App app = new App();
		//myBlockingQueue is a shared resource between many threads
		BlockingQueue<String> myBlockingQueue = new BlockingQueue<String>(1);
		
		QueueProducer producer1 = app.new QueueProducer (myBlockingQueue, "1", "producer1");
		QueueProducer producer2 = app.new QueueProducer (myBlockingQueue, "2", "producer2");
		QueueProducer producer3 = app.new QueueProducer (myBlockingQueue, "3", "producer3");	
		QueueProducer producer4 = app.new QueueProducer (myBlockingQueue, "4", "producer4");	
		QueueProducer producer5 = app.new QueueProducer (myBlockingQueue, "5", "producer5");	

		QueueConsumer consumer = app.new QueueConsumer(myBlockingQueue,"consumer");
		
		try{
			producer1.start();
			Thread.sleep(100);
			producer2.start();
			Thread.sleep(100);
			producer3.start();
			Thread.sleep(100);
			producer4.start();
			Thread.sleep(100);
			producer5.start();
			Thread.sleep(100);
			consumer.start();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	class QueueConsumer extends Thread{
		
		private BlockingQueue<String> bQueue;
		private String threadName;
		
		public QueueConsumer(BlockingQueue<String> queue, String ThreadNameP){
			bQueue = queue;
			threadName = ThreadNameP;
		}
		public void run(){
			String str =null;
			try {
				str = bQueue.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("THREAD-"+threadName+ " -->Taken: "+str);
		}
	}
	
	class QueueProducer extends Thread{
		
		private BlockingQueue<String> bQueue;
		private String eltToAdd;
		private String threadName;
		
		public QueueProducer(BlockingQueue<String> queue, String addedElement, String threadNameP){
			bQueue = queue;
			eltToAdd = addedElement;
			threadName = threadNameP;
		}
		
		public void run(){
			try {
				bQueue.put(eltToAdd);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("THREAD-"+threadName+ " -->Added: "+eltToAdd);
		}
	}
}
