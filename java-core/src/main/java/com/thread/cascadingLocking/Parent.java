package com.thread.cascadingLocking;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Parent {

	private ReentrantLock lock = new ReentrantLock();
	
	private List<Child> children = new ArrayList<Child>();

	public List<Child> getChildren() {
		return children;
	}

	public void setChildren(List<Child> children) {
		this.children = children;
	}

	
	public void process(){
		lock.lock();
		
		try{
			//do some stuff here
			Thread.sleep(12000);
			
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
	
	public void addChild(Child child){
		lock.lock();
		try{
			children.add(child);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
}
