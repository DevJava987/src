package com.thread.cascadingLocking;

public class ThreadTryingToModifyChildren implements Runnable {

	private Child child;
	
	public ThreadTryingToModifyChildren (Child childP){
		this.child = childP;
	}
	
	public void run() {
		child.process();
	}

}
