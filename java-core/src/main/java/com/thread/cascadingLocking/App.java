package com.thread.cascadingLocking;

public class App {

	public static void main (String[] args){
		
		Child child1 = new Child();
		Child child2 = new Child();
		Parent parent = new Parent();
		parent.addChild(child1);
		parent.addChild(child2);
		
		
		Thread processingParent = new Thread(new ThreadLockingParent(parent));
		Thread processingChild = new Thread(new ThreadTryingToModifyChildren(child2));
		processingParent.start();
		processingChild.start();
	}
	
}
