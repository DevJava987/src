package com.InnerClasses.NonStaticInnerClasses.MemberInnerClass;



public class EnclosingClass {

	
	private static int staticEnclosingAttribute = 1;
	private int nonStaticEnclosingAttribute = -1;

	public void myEnclosingStaticMethod(){
		NonStaticMemberInnerClass innerClassInstance = new NonStaticMemberInnerClass();
		System.out.println("The enclosing class can acces to the private non-static attributes" +
							" of the static member inner class, but it needs an instance of this" +
							"static member inner class: "+innerClassInstance.nonstaticInnerAttribute);
	}

	
	
	
	/**
	 * The member class can be declared as public, private, protected, final and abstract
	 */
	class NonStaticMemberInnerClass{
		
		//Can not have static attribute or method because it is not
		//a static member inner class!!!
//		private static int staticInnerAttribute = 99;
		
		private int nonstaticInnerAttribute = -99;
		
		public void nonStaticMethod(){
			
			System.out.println("non Static Method of the non-static member inner calss");
			
			System.out.println("the non-static member inner class can access the static attribut" +
								" of the enclosing class: "+ staticEnclosingAttribute);
			
			EnclosingClass enclosingClasseInstance = new EnclosingClass();
			System.out.println("the non-static member inner class can access even the private" +
								" non-static attribut of the enclosing class but needs an" +
								" instance of this last: "+ enclosingClasseInstance.nonStaticEnclosingAttribute);//this would not be possible within any other top-level class
		}
		
		

		//NOT possible, because static methods can only be defined in
		//the top-level class or inner classes declared as static member!
//		public static void staticMethod(){
//		}
		

		
	}
	
}
