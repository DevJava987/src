package com.InnerClasses.NonStaticInnerClasses.MemberInnerClass;


public class App {
	
	
	// An instance of the enclosing class is needed to instanciate the inner class
	EnclosingClass enclosingClass = new EnclosingClass();
	EnclosingClass.NonStaticMemberInnerClass inner = enclosingClass.new NonStaticMemberInnerClass();

	//NOT possible! because it is not static member inner class
//	EnclosingClass.NonStaticMemberInnerClass i = new EnclosingClass.NonStaticMemberInnerClass();

	
}
