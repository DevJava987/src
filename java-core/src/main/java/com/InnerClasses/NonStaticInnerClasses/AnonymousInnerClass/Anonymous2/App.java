package com.InnerClasses.NonStaticInnerClasses.AnonymousInnerClass.Anonymous2;

public class App {

	public void method(){
		//do something
		MyInterface anonymous = new MyInterface(){
			public void process(){
				System.out.println("process of the anonymous class");
			}
		};
		
	}
	
	
}
