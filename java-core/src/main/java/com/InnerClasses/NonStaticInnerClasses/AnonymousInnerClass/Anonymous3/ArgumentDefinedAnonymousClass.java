package com.InnerClasses.NonStaticInnerClasses.AnonymousInnerClass.Anonymous3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class ArgumentDefinedAnonymousClass {

	public void method() {
		JButton button = new JButton();
		//the method addActionListener needs an instance 
		//of the interface ActionListener
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				System.out.println("Anonymous class implementing the interface ActionListener");
			}
		});
	}
}
