package com.InnerClasses.NonStaticInnerClasses.AnonymousInnerClass.Anonymous1;

public class App {

	
	public void method(){
		SuperClass anonymousClass = new SuperClass(){
			public void process(){
				System.out.println("process of the anonymous class");
			}
		};
		// then user th instance anonymousClass to do something interesting!
	}
	
}
