package com.InnerClasses.StaticInnerClass;

public class EnclosingClass {
	
	private static int staticEnclosingAttribute = 1;
	private int nonStaticEnclosingAttribute = -1;

	
	public void myEnclosingStaticMethod(){
		System.out.println("The enclosing class can acces to the static attribute of the static member inner class: "+StaticMemberInnerClass.staticInnerAttribute);
		
		StaticMemberInnerClass innerClassInstance = new StaticMemberInnerClass();
		System.out.println("The enclosing class can acces to the private non-static attributes" +
							" of the static member inner class, but it needs an instance of this" +
							"static member inner class: "+innerClassInstance.nonstaticInnerAttribute);
	}
	
	
	/**
	 * - The static member inner class can have static and non static methods.
	 * - The static member inner class can access the private static attribute
	 *   of the enclosing class.
	 * - The static member inner class can access even the private non-static 
	 *   attributes of the enclosing class but through an instance of this last.
	 *
	 */
	static class StaticMemberInnerClass{
		
		private static int staticInnerAttribute = 99;
		private int nonstaticInnerAttribute = -99;
		
		public void nonStaticMethod(){
			
			System.out.println("non Static Method of the static member calss");
			
			//A static member class behaves much like an ordinary top-level class,
			//except that it can access the static members of the class that contains it
			System.out.println("the static member inner class can access the static attribut" +
								" of the enclosing class: "+ staticEnclosingAttribute);
			
			EnclosingClass enclosingClasseInstance = new EnclosingClass();
			System.out.println("the static member inner class can access even the private" +
								" non-static attribut of the enclosing class but needs an" +
								" instance of this last: "+ enclosingClasseInstance.nonStaticEnclosingAttribute);//this would not be possible within any other top-level class
		}
		
		public static void staticMethod(){
			System.out.println("static method of the static member inner class");
			System.out.println("the static member inner class can access the static attribut of the enclosing class: "+ staticEnclosingAttribute);

			EnclosingClass enclosingClasseInstance = new EnclosingClass();
			System.out.println("the static member inner class can access even the private" +
								" attribut of the enclosing class but needs an isntance of this last: "+ enclosingClasseInstance.nonStaticEnclosingAttribute);
		}
		
		
	}
	

}
