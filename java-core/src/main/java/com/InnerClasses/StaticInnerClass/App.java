package com.InnerClasses.StaticInnerClass;

public class App {

	static public void main (String ...args){
		
		//non need to use EnclosingClass instance to instantiate the innerClass
		EnclosingClass.StaticMemberInnerClass innerClass = new EnclosingClass.StaticMemberInnerClass();
		
		// NOT possible
//		EnclosingClass enclosingClass = new EnclosingClass();
//		EnclosingClass.StaticMemberInnerClass inner = enclosingClass.new StaticMemberInnerClass();
	}
	
	
}
