package com.sax.advancedsax;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MySaxHandler extends DefaultHandler {
		 
	    //List to hold Employees object
	    private List<Employee> empList = null;
	    private Employee emp = null;
	    private Role role = null;
	 
	 
	    //getter method for employee list
	    public List<Employee> getEmpList() {
	        return empList;
	    }
	 
	    boolean bAge = false;
	    boolean bName = false;
	    boolean bGender = false;
	    boolean bStartDate = false;
	    boolean bEndDate = false;
	 
	    @Override
	    public void startElement(String uri, String localName, String qName, Attributes attributes)
	            throws SAXException {
	 
	        if (qName.equalsIgnoreCase("Employee")) {
	            //create a new Employee and put it in Map
	            String id = attributes.getValue("id");
	            String nid = attributes.getValue("nid");
	            //initialize Employee object and set id attribute
	            emp = new Employee();
	            emp.setId(Integer.parseInt(id));
	            emp.setNid(Integer.parseInt(nid));
	            //initialize list
	            if (empList == null)
	                empList = new ArrayList<Employee>();
	        }else if(qName.equalsIgnoreCase("Role")){
	        	String roleName = attributes.getValue("roleName");
	        	role = new Role();
	        	role.setRoleName(roleName);
	        } else if (qName.equalsIgnoreCase("name")) {
	            //set boolean values for fields, will be used in setting Employee variables
	            bName = true;
	        } else if (qName.equalsIgnoreCase("age")) {
	            bAge = true;
	        } else if (qName.equalsIgnoreCase("gender")) {
	            bGender = true;
	        } else if (qName.equalsIgnoreCase("startDate")){
	        	bStartDate = true;
	        } else if (qName.equalsIgnoreCase("endDate")){
	        	bEndDate = true;
	        }
	    }
	 
	 
	    @Override
	    public void endElement(String uri, String localName, String qName) throws SAXException {
	        if (qName.equalsIgnoreCase("Employee")) {
	            //add Employee object to list
	            empList.add(emp);
	        }else if(qName.equalsIgnoreCase("Role")){
	        	emp.addRole(role);
	        }
	    }
	 
	    @Override
	    public void characters(char ch[], int start, int length) throws SAXException {
	 
	        if (bAge) {
	            //age element, set Employee age
	            emp.setAge(Integer.parseInt(new String(ch, start, length)));
	            bAge = false;
	        } else if (bName) {
	            emp.setName(new String(ch, start, length));
	            bName = false;
	        } else if (bGender) {
	            emp.setGender(new String(ch, start, length));
	            bGender = false;
	        }else if (bStartDate){
	        	role.setStartDate(new String (ch, start, length));
	        	bStartDate = false;
	        }else if (bEndDate){
	        	role.setEndDate(new String (ch, start, length));
	        	bEndDate = false;
	        }
	    }
	}
