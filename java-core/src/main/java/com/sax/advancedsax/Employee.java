package com.sax.advancedsax;

import java.util.ArrayList;
import java.util.List;

public class Employee {
	
	private int id;
	private int nid;
	private String name;
	private String gender;
	private int age;
	private List<Role> roles = new ArrayList<Role>();

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void addRole(Role role){
		this.roles.add(role);
	}
	
	public int getNid() {
		return nid;
	}

	public void setNid(int nid) {
		this.nid = nid;
	}

	@Override
	public String toString() {
		return "Employee:: ID="+this.id+" NID="+this.nid+" Name=" + this.name + " Age=" + this.age + " Gender=" + this.gender +
				" Role=" + this.roles;
	}

}