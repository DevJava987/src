package com.sax;

import java.util.ArrayList;
import java.util.List;

public class Role {

	private String name;
	private String startDate;
	private String endDate;
	private List<Employee> employees = new ArrayList<Employee>();
	
	public String getName() {
		return name;
	}
	
	public void setName(String roleName) {
		this.name = roleName;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public List<Employee> getEmployees() {
		return employees;
	}
	
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void addEmployee(Employee employee){
		this.employees.add(employee);
	}
	
	@Override
	public String toString(){
		 return "{roleName:"+this.name+" startDate:"+this.startDate+" endDate:"+this.endDate+
				 "Employees:"+this.getEmployees()+"}";
	}
}
