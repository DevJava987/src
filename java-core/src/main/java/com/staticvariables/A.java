package com.staticvariables;




public class A {
	
		protected static String value = "A" ;

		public void printValue() {
			System.out.println(value);
		}

	public static void main(String[] args) {
		new B().printValue();
		new A().printValue();// will dislplay B because in the line num 1, value has been set to "B"
							 // the attribute value is static so it is shared between all instance of classe A

	}


}






