package com.connectionBySocket.simpleExample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public static void main (String[] args){
		
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }
		
		String hostname = args[0];
		int serverPort = Integer.parseInt(args[1]);
		try {
			Socket socket = new Socket(hostname, serverPort);
			PrintWriter out= new PrintWriter (socket.getOutputStream());
			BufferedReader in = new BufferedReader (new InputStreamReader(socket.getInputStream()));
			BufferedReader stdIn = new BufferedReader (new InputStreamReader(System.in));
			String userInput = null;
			while ( (userInput = stdIn.readLine()) != null){
				out.println(userInput);
				
				out.flush();
				System.out.println("CLIENT --> SERVER: "+userInput);
				System.out.println("Server response: " +in.readLine());
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
