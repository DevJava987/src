package com.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class App {

	public static void main (String ...args){
		
		Address address = new Address("Paris", "rivoli");
		Person person = Person.createPerson ("fran�ois",32, address);
		
		Class<? extends Person> cl = person.getClass();
		
		Method[] methods = cl.getMethods();//getDeclaredMethods()
		for (int i =0; i < methods.length; i++){
			System.out.println(methods[i].getModifiers());
		}
		
		try {
			System.out.println(cl.getMethod("getName"));
			Method getNameMethod = cl.getMethod("getName");
			getNameMethod.invoke(person, (Object[])null);
			
			Class<?>[] arg = new Class[]{String.class};
			Object[] argForSaySomethingMethod = {"Hello"};
			System.out.println(cl.getMethod("saySomething",  arg));
			Method saySomethingMethod = cl.getMethod("saySomething", arg);
			saySomethingMethod.invoke(person, argForSaySomethingMethod);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (Exception e ){
			e.printStackTrace();
		}
		
		
		// cl.getDeclaredConstructor() returns all constructs even private, protected, default access ones
		// cl.getConstructs() return only public constructs
		Class<?>[] arg = new Class[]{String.class, int.class, Address.class};
		try{System.out.println(cl.getDeclaredConstructor(arg));}catch(NoSuchMethodException e){e.printStackTrace();}
		
		
		try{System.out.println(cl.getDeclaredField("address"));}catch (NoSuchFieldException e){e.printStackTrace();}
		
		try{
			Field addressField = cl.getField("address");
			Class<?> clAddress = addressField.getDeclaringClass();
		}catch(NoSuchFieldException e){}
		
		//do the same work with clAddress as the cl object
		
		Field[] fields = cl.getFields();
		for (int i = 0; i < fields.length; i++){
			System.out.println(fields[i]);
		}
		
	
	}
	
}
