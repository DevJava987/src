package com.aam.training.order.provider;

import com.aam.training.order.Order;

public interface MyProvider {

    Order retrieveOrder(String orderId);
}
