package com.aam.training.order;

import static com.aam.training.order.ORDER_STATUS.FILL;
import static com.aam.training.order.ORDER_STATUS.NEW;

public class Order {

    private int askedQty;
    private String orderId;
    private ORDER_STATUS orderStatus = NEW;
    private int execQty;
    private double px;


    public Order(){
    }

    public int getExecQty() {
        return execQty;
    }

    public void setExecQty(int execQty) {
        this.execQty = execQty;
    }

    public double getPx() {
        return px;
    }

    public void setPx(double px) {
        this.px = px;
    }

    public int getAskedQty() {
        return askedQty;
    }

    public void setAskedQty(int askedQty) {
        this.askedQty = askedQty;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public ORDER_STATUS getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(ORDER_STATUS orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean isFullyExecuted() {
        return FILL.equals(orderStatus);
    }
}
