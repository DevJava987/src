package com.aam.training.order;

import com.aam.training.order.provider.MyProvider;

public class Process {

    //Injection
    private MyProvider provider;

    public void handleExecution(String orderId, int execQty, double px) throws OverExecException {
        Order order = retrieveOrder(orderId);
        if (order.getAskedQty() < (order.getExecQty() + execQty)) {
            throw new OverExecException();
        }
        updateOrder(order, execQty, px);
        if (BookingHandler.isBookingSystemUp()) {
            handleBooking(order);
        }
        logInDatabase();
        privateVoidMethodToBeMocked();
        privateBoolMethodToBeMocked();
    }

    public void updateOrder(Order order, Integer execQty, Double px) {
        order.setExecQty(order.getExecQty() + execQty);
        order.setPx(px);
        if (order.getAskedQty() == order.getExecQty()) {
            order.setOrderStatus(ORDER_STATUS.FILL);
        } else {
            order.setOrderStatus(ORDER_STATUS.PART);
        }
    }

    public Order retrieveOrder(String orderId) {
        return provider.retrieveOrder(orderId);
    }

    public void setProvider(MyProvider provider) {
        this.provider = provider;
    }

    private void handleBooking(Order order) {
        if (order.isFullyExecuted()) {
            BookingHandler.generateBooking(order);
        }
    }

    public void logInDatabase() {
        throw new UnsupportedOperationException("logInDatabase should not be called because mocked");
    }

    private void privateVoidMethodToBeMocked() {
        throw new UnsupportedOperationException("privateVoidMethodToBeMocked should not be called because mocked");
    }

    private boolean privateBoolMethodToBeMocked() {
        throw new UnsupportedOperationException("privateBoolMethodToBeMocked should not be called because mocked");
    }
}
