package com.aam.training.order;


import com.aam.training.order.booking.BookingHandler;
import com.aam.training.order.provider.MyProvider;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.testng.PowerMockTestCase;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Process.class, Order.class, MyProvider.class, BookingHandler.class})

// In addition to mentioning the class in the PrepareForTest,
// we have to extend PowerMockTestCase to enable mocking static
public class OrderWorkflowTesting extends PowerMockTestCase {


    @BeforeClass
    public void setUp(){
        //Initializes objects annotated with Mockito annotations for given testClass
        //(Spy Mock, Capture and injectMock)
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecAddQtyAndPrice() throws Exception{
        String orderId = "MyOrderId"; int execQty = 10; int askedQty = 10; double px = 5.5;
        Order order = new Order();order.setOrderId(orderId);order.setAskedQty(askedQty);

        Process spyProcess = PowerMockito.spy(new Process());
        MyProvider mockedProvider = PowerMockito.mock(MyProvider.class);
        spyProcess.setProvider(mockedProvider);
        PowerMockito.mockStatic(BookingHandler.class);

        // #### MOCKING A "COLLABORATOR"
        when(mockedProvider.retrieveOrder(orderId)).thenReturn(order);
        //PowerMockito.doNothing().when(BookingHandler.class, "generateBooking", order);

        // #### MOCKING A STATIC METHODS
        // 1- Mocking a static method that return void
        PowerMockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                System.out.println("doAnswer is called");
                return null;
            }
        }).when(BookingHandler.class, "generateBooking", order );
        // 2- Mocking s static method that returns a value
        PowerMockito.when(BookingHandler.isBookingSystemUp()).thenReturn(true);

        // ##### STUBBING specific method of SPY
        doNothing().when(spyProcess).logInDatabase();

        // ##### MOCKING private methods
        //With PowerMockito to stub private methods you should start by "doReturn/doNothing/DoAnswer..." and then you call "when()"
        PowerMockito.doReturn(true).when(spyProcess, "privateBoolMethodToBeMocked");
        //PowerMockito.doNothing().when(spyProcess,"privateVoidMethodToBeMocked" );
        PowerMockito.doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                // Nothing to do to be equivalent of PowerMockito.doNothing
                return null;
            }
        }).when(spyProcess, "privateVoidMethodToBeMocked");


        // ASSERT STATE HERE BEFORE HANDLING THE EXECUTION
        Assert.assertEquals(order.getOrderStatus(),ORDER_STATUS.NEW);

        //HERE IS THE METHOD TO TEST and VERIFY
        spyProcess.handleExecution(orderId, execQty, px);

        // Check Asserts
        Assert.assertEquals(order.getExecQty(), execQty);
        Assert.assertEquals(order.getPx(), px);
        Assert.assertEquals(order.getOrderStatus(),ORDER_STATUS.FILL);

        Mockito.verify(spyProcess, times(1)).retrieveOrder(orderId);
        Mockito.verify(spyProcess, times(1)).updateOrder(order, execQty,px );

        //To verify a call to a static method, the latter should be called just after the check as bellow
        //There will be no check if you write the first line only!!!
        PowerMockito.verifyStatic(BookingHandler.class, times(1));
        BookingHandler.isBookingSystemUp();

        // #### VERIFYING calls to private method
        PowerMockito.verifyPrivate(spyProcess,times(1)).invoke("handleBooking",order);

        Mockito.verify(spyProcess, times(1)).logInDatabase();

        //Order of method calls
        InOrder orderVerifier = Mockito.inOrder(spyProcess);
        orderVerifier.verify(spyProcess).retrieveOrder(orderId);
        orderVerifier.verify(spyProcess).updateOrder(order, execQty, px);
        orderVerifier.verify(spyProcess).logInDatabase();

    }


    @Test(expectedExceptions = OverExecException.class)
    public void testOverExec()throws Exception {
        String orderId = "MyOrderId"; int execQty = 10; int askedQty = 7; double px = 5.5;
        Order order = new Order();order.setOrderId(orderId);order.setAskedQty(askedQty);

        Process spyProcess = PowerMockito.spy(new Process());
        MyProvider mockedProvider = PowerMockito.mock(MyProvider.class);
        spyProcess.setProvider(mockedProvider);
        when(mockedProvider.retrieveOrder(orderId)).thenReturn(order);

        try {
            Assert.assertEquals(order.getOrderStatus(),ORDER_STATUS.NEW );
            spyProcess.handleExecution(orderId, execQty, px);
        } catch (OverExecException oee) {
            Assert.assertEquals(order.getOrderStatus(),ORDER_STATUS.NEW );
            Mockito.verify(spyProcess, times(1)).retrieveOrder(orderId);
            Mockito.verify(spyProcess, times(0)).updateOrder(order, execQty,px );
            Mockito.verify(spyProcess, times(0)).logInDatabase();
            PowerMockito.verifyStatic(BookingHandler.class, times(0));
            throw oee;
        }
    }



    /**################################### REMARKS ###########################################
     *
     *   Useful links talking about Mockito and PowerMockito:
     *   https://www.geeklearners.com/2019/03/understanding-mockito-and-powermockito.html
     *
     * -1) DEFAULTING : By default, for all methods that return a value, a mock will return either null,
     * a primitive/primitive wrapper value, or an empty collection, as appropriate.
     * For example 0 for an int/Integer and false for a boolean/Boolean
     *
     * -2) VOID METHOD : you can use the doNothing, doAnswer or doThrow on a mock/spy object
     *  2.1 on concrete method examples:
     *      - PowerMockito.doNothing().when(mock/spy).someVoidMethod()
     *      - PowerMockito.doThrow().when(mock/spy).someVoidMethod()
     *      - PowerMockito.doAnswer(new Answer(){...}).when(mock/spy).someVoidMethod()
     *  2.2 Do not forget to do "PowerMockito.staticMock(class)" in case of static method. Examples
     *      - PowerMockito.doNothing().when(MyClass.class, "methodNAme", Arguments...)
     *      - PowerMockito.doThrow().when(MyClass.class, "methodNAme", Arguments...)
     *      - PowerMockito.doAnswer(new Answer(){...}).when(MyClass.class, "methodNAme", Arguments...)
     *
     *   3) Static methods
     *   To mock/stub static method you have to make PowerMockito.staticMock(MyClass.class)
     *   Pay attention, this will DEFAULTLY mock ALL static methods. For example,
     *   void static method will be mocked by doNothing. static method returning bool will return false...
     *   You can customize this behaviour as follow:
     *   + for void methods:
     *         - PowerMockito.doNothing().when(MyClass.class, "methodNAme", Arguments...)
     *         - PowerMockito.doThrow().when(MyClass.class, "methodNAme", Arguments...)
     *         - PowerMockito.doAnswer(new Answer(){...}).when(MyClass.class, "methodNAme", Arguments...)
     *    + For none void method mock them classically:
     *         - PowerMockito.when(MyClass.somVoidMethod).thenReturn(...);
     *
     *    4) InOrder: a Mockito feature that helps check the order in which methods were called.
     *    An instance of InOrder can used to check the call order of methods of one and only Mock/Spy
       #######################################################################################**/

}
