### Standard JDBC API has the following disadvantages:
- Apart from executing the main query, you need to write a lot of code to handle the execution environment issues such as 
creating connection, statement, resultset, etc.
- Need to handle exception handling code separately.
- Need to handle transactional issues.
--> See more at: http://www.devx.com/Java/how-to-access-relational-data-using-jdbc-with-spring.html#sthash.Zm8xZJ9e.dpuf

### Spring JDBC template has the following advantages compared with the normal approach of standard JDBC.

- Cleaning of used resources is done automatically by the Spring JDBC template. 
	Developers do not need to bother about releasing the resources. Hence it prevents memory leaks.
- Spring JDBC template handles the exception and errors in a more efficient way. 
	It converts the JDBC SQLExceptions into RuntimeExceptions, so the developers can handle it in more flexible manner.
- The Spring JDBC template also converts the vendor specific errors in a more meaningful message, 
  thus the handling of those errors is more efficient.

--> See more at: http://www.devx.com/Java/how-to-access-relational-data-using-jdbc-with-spring.html#sthash.Zm8xZJ9e.dpuf


The JdbcTemplate class is the central class in the JDBC core package. It handles the creation and release of resources,
which helps you avoid common errors such as forgetting to close the connection. 
It performs the basic tasks of the core JDBC workflow such as statement creation and execution, 
leaving application code to provide SQL and extract results. The 
JdbcTemplate class executes SQL queries, update statements and stored procedure calls, performs iteration over ResultSets
and extraction of returned parameter values. It also catches JDBC exceptions and translates them to the generic, more informative, 
exception hierarchy defined in the org.springframework.dao package.

When you use the JdbcTemplate for your code, you only need to implement callback interfaces, giving them a clearly defined contract. 
The PreparedStatementCreator callback interface creates a prepared statement given a Connection provided by this class, providing SQL 
and any necessary parameters. The same is true for the CallableStatementCreator interface, which creates callable statements. 
The RowCallbackHandler interface extracts values from each row of a ResultSet.

