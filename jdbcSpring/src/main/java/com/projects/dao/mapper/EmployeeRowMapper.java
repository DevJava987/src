package com.projects.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.projects.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {

	public Employee mapRow(ResultSet paramResultSet, int paramInt)	throws SQLException {
		int id = paramResultSet.getInt("ID");
		String name = paramResultSet.getString("NAME");
		int age = paramResultSet.getInt("AGE");
		Employee emp = new Employee(id, name, age);
		return emp;
	}

}
