package com.projects;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
public class Sender implements Runnable{

	JmsTemplate jmsTemplate;

	public void setJmsTemplate(JmsTemplate jmsTemplate){
		this.jmsTemplate = jmsTemplate;
	}

	public void run() {
		MessageCreator msgCreator = new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				TextMessage msg =session.createTextMessage("coucou");
				return msg;
			}
		};
		for (int i = 0; i<10; i++){
			jmsTemplate.send(msgCreator);
			//To use this method the encapsulated object should be serializable
			jmsTemplate.convertAndSend(new Notification("Hello"));
			try {
				Thread.sleep(1100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
