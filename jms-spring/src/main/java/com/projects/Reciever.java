package com.projects;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;

public class Reciever implements Runnable{

	JmsTemplate jmsTemplate;

	public void setJmsTemplate(JmsTemplate jmsTemplate){
		this.jmsTemplate = jmsTemplate;
	}

	public void run() {
		while (!Thread.interrupted()){
			try {
				Message msg = jmsTemplate.receive();
				if (msg instanceof ObjectMessage){
					System.out.println(((ObjectMessage) msg).getObject().toString());
				}else if (msg instanceof TextMessage){
					System.out.println("recieved msg: "+((TextMessage)msg).getText());	
				}
			} catch (Exception jmsE){
				Thread.currentThread().interrupt();
			}
		}
		System.out.println("Thread reciever stopped");
	}

}
