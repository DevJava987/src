package com.projects;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {

	public static void main( String[] args ){
		
		ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		List<String> list = new ArrayList<>();
		
		Thread sender = new Thread ((Sender) ctx.getBean("sender"));
		Thread reciever = new Thread ((Reciever) ctx.getBean("reciever"));
		sender.start();
		reciever.start();

		
		try {
			sender.join();
			reciever.interrupt();
			reciever.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ctx.close();
	}
}
